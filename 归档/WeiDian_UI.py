# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'WeiDian.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1229, 853)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(570, 10, 651, 501))
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.formLayoutWidget_2 = QtWidgets.QWidget(self.tab)
        self.formLayoutWidget_2.setGeometry(QtCore.QRect(320, 20, 261, 167))
        self.formLayoutWidget_2.setObjectName("formLayoutWidget_2")
        self.formLayout_2 = QtWidgets.QFormLayout(self.formLayoutWidget_2)
        self.formLayout_2.setContentsMargins(0, 0, 0, 0)
        self.formLayout_2.setObjectName("formLayout_2")
        self.label_code_err_count = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.label_code_err_count.setObjectName("label_code_err_count")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_code_err_count)
        self.lineEdit_code_retry = QtWidgets.QLineEdit(self.formLayoutWidget_2)
        self.lineEdit_code_retry.setObjectName("lineEdit_code_retry")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.lineEdit_code_retry)
        self.label_shiming_retry = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.label_shiming_retry.setObjectName("label_shiming_retry")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_shiming_retry)
        self.lineEdit_shiming_retry = QtWidgets.QLineEdit(self.formLayoutWidget_2)
        self.lineEdit_shiming_retry.setObjectName("lineEdit_shiming_retry")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.lineEdit_shiming_retry)
        self.label_18 = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.label_18.setObjectName("label_18")
        self.formLayout_2.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_18)
        self.lineEdit_ip_err_retry = QtWidgets.QLineEdit(self.formLayoutWidget_2)
        self.lineEdit_ip_err_retry.setObjectName("lineEdit_ip_err_retry")
        self.formLayout_2.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.lineEdit_ip_err_retry)
        self.label_19 = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.label_19.setObjectName("label_19")
        self.formLayout_2.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_19)
        self.lineEdit_image_code_err_retry = QtWidgets.QLineEdit(self.formLayoutWidget_2)
        self.lineEdit_image_code_err_retry.setObjectName("lineEdit_image_code_err_retry")
        self.formLayout_2.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.lineEdit_image_code_err_retry)
        self.label_20 = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.label_20.setObjectName("label_20")
        self.formLayout_2.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_20)
        self.lineEdit_data_use_count = QtWidgets.QLineEdit(self.formLayoutWidget_2)
        self.lineEdit_data_use_count.setObjectName("lineEdit_data_use_count")
        self.formLayout_2.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.lineEdit_data_use_count)
        self.checkBox_sms_register = QtWidgets.QCheckBox(self.tab)
        self.checkBox_sms_register.setGeometry(QtCore.QRect(40, 210, 87, 20))
        self.checkBox_sms_register.setObjectName("checkBox_sms_register")
        self.checkBox_register_find_password = QtWidgets.QCheckBox(self.tab)
        self.checkBox_register_find_password.setGeometry(QtCore.QRect(40, 240, 211, 21))
        self.checkBox_register_find_password.setObjectName("checkBox_register_find_password")
        self.checkBox_find_pasword_login = QtWidgets.QCheckBox(self.tab)
        self.checkBox_find_pasword_login.setGeometry(QtCore.QRect(40, 270, 211, 21))
        self.checkBox_find_pasword_login.setObjectName("checkBox_find_pasword_login")
        self.checkBox_register_success_shiming = QtWidgets.QCheckBox(self.tab)
        self.checkBox_register_success_shiming.setGeometry(QtCore.QRect(40, 300, 211, 21))
        self.checkBox_register_success_shiming.setObjectName("checkBox_register_success_shiming")
        self.checkBox_find_password_shiming = QtWidgets.QCheckBox(self.tab)
        self.checkBox_find_password_shiming.setGeometry(QtCore.QRect(40, 330, 211, 21))
        self.checkBox_find_password_shiming.setObjectName("checkBox_find_password_shiming")
        self.checkBox_use_ip = QtWidgets.QCheckBox(self.tab)
        self.checkBox_use_ip.setGeometry(QtCore.QRect(40, 360, 211, 21))
        self.checkBox_use_ip.setObjectName("checkBox_use_ip")
        self.checkBox_password_login = QtWidgets.QCheckBox(self.tab)
        self.checkBox_password_login.setGeometry(QtCore.QRect(340, 210, 211, 21))
        self.checkBox_password_login.setObjectName("checkBox_password_login")
        self.change_phone = QtWidgets.QCheckBox(self.tab)
        self.change_phone.setGeometry(QtCore.QRect(340, 300, 211, 21))
        self.change_phone.setObjectName("change_phone")


        
        self.checkBox_login_query_code = QtWidgets.QCheckBox(self.tab)
        self.checkBox_login_query_code.setGeometry(QtCore.QRect(340, 240, 211, 21))
        self.checkBox_login_query_code.setObjectName("checkBox_login_query_code")
        self.pushButton_stop = QtWidgets.QPushButton(self.tab)
        self.pushButton_stop.setGeometry(QtCore.QRect(100, 400, 113, 32))
        self.pushButton_stop.setObjectName("pushButton_stop")
        self.pushButton_start = QtWidgets.QPushButton(self.tab)
        self.pushButton_start.setGeometry(QtCore.QRect(380, 400, 113, 32))
        self.pushButton_start.setObjectName("pushButton_start")
        self.pushButton_refresh = QtWidgets.QPushButton(self.tab)
        self.pushButton_refresh.setGeometry(QtCore.QRect(510, 400, 113, 32))
        self.pushButton_refresh.setObjectName("pushButton_refresh")
        
        self.formLayoutWidget = QtWidgets.QWidget(self.tab)
        self.formLayoutWidget.setGeometry(QtCore.QRect(20, 20, 251, 161))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label_register_count = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_register_count.setObjectName("label_register_count")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_register_count)
        self.lineEdit_register_count = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.lineEdit_register_count.setObjectName("lineEdit_register_count")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.lineEdit_register_count)
        
        self.label_thread_count = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_thread_count.setObjectName("label_thread_count")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_thread_count)
        
        self.lineEdit_thread_count = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.lineEdit_thread_count.setObjectName("lineEdit_thread_count")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.lineEdit_thread_count)
        self.label_register_password = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_register_password.setObjectName("label_register_password")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_register_password)


        
        
        self.lineEdit_register_password = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.lineEdit_register_password.setText("")
        self.lineEdit_register_password.setObjectName("lineEdit_register_password")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.lineEdit_register_password)
        self.label_login_count = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_login_count.setObjectName("label_login_count")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_login_count)
        self.lineEdit_login_count = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.lineEdit_login_count.setObjectName("lineEdit_login_count")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.lineEdit_login_count)
        self.label_login_sleep = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_login_sleep.setObjectName("label_login_sleep")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_login_sleep)

        self.label_certno_count = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_certno_count.setObjectName("label_certno_count")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_certno_count)
        
        
        self.lineEdit_login_sleep = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.lineEdit_login_sleep.setObjectName("lineEdit_login_sleep")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.lineEdit_login_sleep)
        
        self.lineEdit_certno_count = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.lineEdit_certno_count.setObjectName("lineEdit_certno_count")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.lineEdit_certno_count)


        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.formLayoutWidget_3 = QtWidgets.QWidget(self.tab_2)
        self.formLayoutWidget_3.setGeometry(QtCore.QRect(90, 30, 241, 121))
        self.formLayoutWidget_3.setObjectName("formLayoutWidget_3")
        self.formLayout_3 = QtWidgets.QFormLayout(self.formLayoutWidget_3)
        self.formLayout_3.setContentsMargins(0, 0, 0, 0)
        self.formLayout_3.setObjectName("formLayout_3")
        self.label_23 = QtWidgets.QLabel(self.formLayoutWidget_3)
        self.label_23.setObjectName("label_23")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_23)
        self.comboBox_platform = QtWidgets.QComboBox(self.formLayoutWidget_3)
        self.comboBox_platform.setObjectName("comboBox_platform")
        self.comboBox_platform.addItem("")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.comboBox_platform)
        self.label_24 = QtWidgets.QLabel(self.formLayoutWidget_3)
        self.label_24.setObjectName("label_24")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_24)
        self.lineEdit_code_account = QtWidgets.QLineEdit(self.formLayoutWidget_3)
        self.lineEdit_code_account.setObjectName("lineEdit_code_account")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.lineEdit_code_account)
        self.label_25 = QtWidgets.QLabel(self.formLayoutWidget_3)
        self.label_25.setObjectName("label_25")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_25)
        self.lineEdit_code_password = QtWidgets.QLineEdit(self.formLayoutWidget_3)
        self.lineEdit_code_password.setObjectName("lineEdit_code_password")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.lineEdit_code_password)
        self.label_26 = QtWidgets.QLabel(self.formLayoutWidget_3)
        self.label_26.setObjectName("label_26")
        self.formLayout_3.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_26)
        self.lineEdit_code_project_id = QtWidgets.QLineEdit(self.formLayoutWidget_3)
        self.lineEdit_code_project_id.setObjectName("lineEdit_code_project_id")
        self.formLayout_3.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.lineEdit_code_project_id)
        self.pushButton_code_login = QtWidgets.QPushButton(self.tab_2)
        self.pushButton_code_login.setGeometry(QtCore.QRect(160, 190, 113, 32))
        self.pushButton_code_login.setObjectName("pushButton_code_login")
        self.checkBox_code_pass_fake_phone = QtWidgets.QCheckBox(self.tab_2)
        self.checkBox_code_pass_fake_phone.setGeometry(QtCore.QRect(160, 230, 181, 16))
        self.checkBox_code_pass_fake_phone.setObjectName("checkBox_code_pass_fake_phone")
        self.textBrowser_code = QtWidgets.QTextBrowser(self.tab_2)
        self.textBrowser_code.setGeometry(QtCore.QRect(10, 260, 631, 201))
        self.textBrowser_code.setObjectName("textBrowser_code")
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.lineEdit_ip = QtWidgets.QLineEdit(self.tab_3)
        self.lineEdit_ip.setGeometry(QtCore.QRect(90, 40, 521, 21))
        self.lineEdit_ip.setObjectName("lineEdit_ip")
        self.pushButton_ip_test = QtWidgets.QPushButton(self.tab_3)
        self.pushButton_ip_test.setGeometry(QtCore.QRect(170, 80, 113, 32))
        self.pushButton_ip_test.setObjectName("pushButton_ip_test")
        self.label_22 = QtWidgets.QLabel(self.tab_3)
        self.label_22.setGeometry(QtCore.QRect(40, 40, 60, 16))
        self.label_22.setObjectName("label_22")
        self.label_ip_hidden = QtWidgets.QLabel(self.tab_3)
        self.label_ip_hidden.setGeometry(QtCore.QRect(50, 140, 571, 251))
        self.label_ip_hidden.setText("")
        self.label_ip_hidden.setObjectName("label_ip_hidden")
        self.tabWidget.addTab(self.tab_3, "")
        self.textBrowser_log = QtWidgets.QTextBrowser(self.centralwidget)
        self.textBrowser_log.setGeometry(QtCore.QRect(10, 520, 1211, 281))
        self.textBrowser_log.setObjectName("textBrowser_log")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(10, 20, 551, 491))
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1229, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_code_err_count.setText(_translate("MainWindow", "打码失败重试"))
        self.label_shiming_retry.setText(_translate("MainWindow", "实名失败重试"))
        self.label_18.setText(_translate("MainWindow", "ip失效重试"))
        self.label_19.setText(_translate("MainWindow", "虚拟卡平台选择"))
        self.label_20.setText(_translate("MainWindow", "手机号换绑次数限制"))
        self.checkBox_sms_register.setText(_translate("MainWindow", "短信注册"))
        self.checkBox_register_find_password.setText(_translate("MainWindow", "注册时_已注册自动找密"))
        self.checkBox_find_pasword_login.setText(_translate("MainWindow", "找密时_先随机密码登录"))
        self.checkBox_register_success_shiming.setText(_translate("MainWindow", "注册成功后_执行实名"))
        self.checkBox_find_password_shiming.setText(_translate("MainWindow", "找密成功后_执行实名"))
        self.checkBox_use_ip.setText(_translate("MainWindow", "操作时_使用ip"))
        self.checkBox_password_login.setText(_translate("MainWindow", "密码登录"))
        self.checkBox_login_query_code.setText(_translate("MainWindow", "登录时_出校验自动取码"))
        self.change_phone.setText(_translate("MainWindow", "是否换绑"))



        self.pushButton_stop.setText(_translate("MainWindow", "停止操作"))
        self.pushButton_start.setText(_translate("MainWindow", "开始操作"))
        self.pushButton_refresh.setText(_translate("MainWindow", "刷新本地资料"))
        self.label_register_count.setText(_translate("MainWindow", "注册数"))
        self.label_thread_count.setText(_translate("MainWindow", "线程数"))
        self.label_register_password.setText(_translate("MainWindow", "注册密码"))

        self.label_login_count.setText(_translate("MainWindow", "登录次数"))
        self.label_login_sleep.setText(_translate("MainWindow", "登录延迟"))
        self.label_certno_count.setText(_translate("MainWindow", "身份证实名次数"))

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "操作"))
        self.label_23.setText(_translate("MainWindow", "平台选择"))
        self.comboBox_platform.setItemText(0, _translate("MainWindow", "椰子云"))
        self.comboBox_platform.addItem("米云")

        self.label_24.setText(_translate("MainWindow", "账号"))
        self.label_25.setText(_translate("MainWindow", "密码"))
        self.label_26.setText(_translate("MainWindow", "项目ID"))
        self.pushButton_code_login.setText(_translate("MainWindow", "登录"))
        self.checkBox_code_pass_fake_phone.setText(_translate("MainWindow", "过滤虚拟手机号"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("MainWindow", "接码配置"))
        self.pushButton_ip_test.setText(_translate("MainWindow", "测试获取"))
        self.label_22.setText(_translate("MainWindow", "代理Ip"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("MainWindow", "ip配置"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "id"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "手机号"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "密码"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "状态"))

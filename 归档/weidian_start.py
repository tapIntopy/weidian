import sys
import requests
import ddddocr
import json, sqlite3
import time, base64
import io, os
import gzip
import traceback
import random
from urllib import parse
import string, json
from requests_toolbelt import MultipartEncoder
from configparser import ConfigParser
from  log import logger
from random import choice
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from hashlib import md5
from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox, QTableWidgetItem
from WeiDian_UI import Ui_MainWindow
from automatic_phone import collectSms

stop_flag = True
mc_random = ["a","1","b","3","4","5","6","f","8","9"]

def get_random_mc():
    mc = '{}:{}:{}:{}:{}:{}'.format("".join(random.choices(mc_random,k=2)),"".join(random.choices(mc_random,k=2)),"".join(random.choices(mc_random,k=2)),"".join(random.choices(mc_random,k=2)),"".join(random.choices(mc_random,k=2)),"".join(random.choices(mc_random,k=2)))
    return mc


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.tabWidget.setCurrentIndex(0)
        # 判断当前是否配置了接码
        self.auto_phone = None
        # 读取配置文件到输入框
        self.read_config()
        self.comboBox_platform.activated[str].connect(self.comboBox_platform_change)



    def read_config(self):
        cf = ConfigParser()
        try:
            try:
                cf.read("./config.ini", encoding='gbk')
            except:
                cf.read("./config.ini", encoding='utf8')
            label_register_count = cf.get('operation', 'label_register_count')
            self.lineEdit_register_count.setText(label_register_count)
            label_thread_count = cf.get('operation', 'label_thread_count')
            self.lineEdit_thread_count.setText(label_thread_count)
            self.lineEdit_register_password.setText(cf.get('operation', 'label_register_password'))
            self.lineEdit_login_count.setText(cf.get('operation', 'label_login_count'))
            self.lineEdit_login_sleep.setText(cf.get('operation', 'label_login_sleep'))
            
            self.lineEdit_certno_count.setText(cf.get('operation', 'label_certno_count'))
            
            self.lineEdit_code_retry.setText(cf.get('operation', 'label_code_err_count'))
            self.lineEdit_shiming_retry.setText(cf.get('operation', 'label_shiming_retry'))
            self.lineEdit_ip_err_retry.setText(cf.get('operation', 'lineEdit_ip_err_retry'))
            self.lineEdit_image_code_err_retry.setText(cf.get('operation', 'lineEdit_image_code_err_retry'))
            self.lineEdit_data_use_count.setText(cf.get('operation', 'lineEdit_data_use_count'))
            # 下面单选框
            checkBox_sms_register = cf.getboolean("operation", "checkBox_sms_register")
            self.checkBox_sms_register.setChecked(checkBox_sms_register)
            checkBox_register_find_password = cf.getboolean("operation", "checkBox_register_find_password")
            self.checkBox_register_find_password.setChecked(checkBox_register_find_password)
            checkBox_find_pasword_login = cf.getboolean("operation", "checkBox_find_pasword_login")
            self.checkBox_find_pasword_login.setChecked(checkBox_find_pasword_login)
            
            change_phone =  cf.getboolean("operation", "change_phone")
            self.change_phone.setChecked(change_phone)
            checkBox_register_success_shiming = cf.getboolean("operation", "checkBox_register_success_shiming")
            self.checkBox_register_success_shiming.setChecked(checkBox_register_success_shiming)
            checkBox_find_password_shiming = cf.getboolean("operation", "checkBox_find_password_shiming")
            self.checkBox_find_password_shiming.setChecked(checkBox_find_password_shiming)
            checkBox_use_ip = cf.getboolean("operation", "checkBox_use_ip")
            self.checkBox_use_ip.setChecked(checkBox_use_ip)
            checkBox_password_login = cf.getboolean("operation", "checkBox_password_login")
            self.checkBox_password_login.setChecked(checkBox_password_login)
            checkBox_login_query_code = cf.getboolean("operation", "checkBox_login_query_code")
            self.checkBox_login_query_code.setChecked(checkBox_login_query_code)
            # 接码配置
            sections = cf.sections()
            #循环读取节点
            comboBox_platform_list = []
            for se in sections:
                #如果是code开头， 读取接码云配置， 塞入到combox里面
                if se.startswith("code"):
                    comboBox_platform_list.append(cf.get(se, "comboBox_platform"))
            self.comboBox_platform.clear() #清空原来得
            self.comboBox_platform.addItems(comboBox_platform_list)
            
            lineEdit_code_account = cf.get("code", "lineEdit_code_account")
            self.lineEdit_code_account.setText(str(lineEdit_code_account))
            lineEdit_code_password = cf.get("code", "lineEdit_code_password")
            self.lineEdit_code_password.setText(str(lineEdit_code_password))
            lineEdit_code_project_id = cf.get("code", "lineEdit_code_project_id")
            self.lineEdit_code_project_id.setText(str(lineEdit_code_project_id))
            checkBox_code_pass_fake_phone = cf.getboolean("code", "checkBox_code_pass_fake_phone")
            self.checkBox_code_pass_fake_phone.setChecked(checkBox_code_pass_fake_phone)
            # ip配置
            lineEdit_ip = cf.get("ip", "lineEdit_ip")
            self.lineEdit_ip.setText(str(lineEdit_ip))
            self.cf = cf

        except Exception as e:
            
            self.textBrowser_log.append(f"读取配置文件失败，错误e=%s" % e)

    def save_init(self):
        config = ConfigParser()
        
        # add section 添加section项
        # set(section,option,value) 给section项中写入键值对
        section_list = ['operation','code', 'code2', 'code3', 'ip' ]
        for s in section_list:
            config.add_section(s)
        operation_label_dict = {
            'label_certno_count' : int(self.lineEdit_certno_count.text().strip()),
            'label_register_count': int(self.lineEdit_register_count.text().strip()),
            'label_thread_count': int(self.lineEdit_thread_count.text().strip()),
            'label_register_password': self.lineEdit_register_password.text().strip(),
            'label_login_count': int(self.lineEdit_login_count.text().strip()),
            'label_login_sleep': self.lineEdit_login_sleep.text().strip(),
            'label_code_err_count': self.lineEdit_code_retry.text().strip(),
            'label_shiming_retry': self.lineEdit_shiming_retry.text().strip(),
            'lineEdit_ip_err_retry': self.lineEdit_ip_err_retry.text(),
            'lineEdit_image_code_err_retry': self.lineEdit_image_code_err_retry.text(),
            'lineEdit_data_use_count': self.lineEdit_data_use_count.text(),
            'checkBox_sms_register': self.checkBox_sms_register.isChecked(),
            'checkBox_register_find_password': self.checkBox_register_find_password.isChecked(),
            'checkBox_find_pasword_login': self.checkBox_find_pasword_login.isChecked(),
            'checkBox_register_success_shiming': self.checkBox_register_success_shiming.isChecked(),
            'checkBox_find_password_shiming': self.checkBox_find_password_shiming.isChecked(),
            'checkBox_use_ip': self.checkBox_use_ip.isChecked(),
            'change_phone': self.change_phone.isChecked(),
            'checkBox_password_login': self.checkBox_password_login.isChecked(),
            'checkBox_login_query_code': self.checkBox_login_query_code.isChecked()
            
    
        }
        now_categoey = self.comboBox_platform.currentText()
        for se in self.cf.sections():
            if se.startswith("code"):
                now_comboBox_platform = self.cf.get(se, "comboBox_platform")
                if now_comboBox_platform == now_categoey:
                    now_code = se


        comboBox_platform = self.cf.get(now_code, "comboBox_platform")
        lineEdit_code_project_id = self.lineEdit_code_project_id.text()
        lineEdit_code_account = self.lineEdit_code_account.text()
        lineEdit_code_password = self.lineEdit_code_password.text()
        checkBox_code_pass_fake_phone = str(self.cf.getboolean(now_code, "checkBox_code_pass_fake_phone"))
        config.set('code', 'comboBox_platform',comboBox_platform)
        config.set('code', 'lineEdit_code_account',lineEdit_code_account)
        config.set('code', 'lineEdit_code_password',lineEdit_code_password)
        config.set('code', 'lineEdit_code_project_id',lineEdit_code_project_id)
        config.set('code', 'checkBox_code_pass_fake_phone',checkBox_code_pass_fake_phone)
        for c in ['code2','code3']:
            if c == now_code:
                read_code = 'code'
            else:
                read_code = c

            lineEdit_code_project_id = self.cf.get(read_code, "lineEdit_code_project_id")
            lineEdit_code_account = self.cf.get(read_code, "lineEdit_code_account")
            lineEdit_code_password = self.cf.get(read_code, "lineEdit_code_password")
            checkBox_code_pass_fake_phone = str(self.cf.getboolean(read_code, "checkBox_code_pass_fake_phone"))
            
            comboBox_platform = self.cf.get(read_code, "comboBox_platform")
            config.set(c, 'comboBox_platform',comboBox_platform)
            config.set(c, 'lineEdit_code_account',lineEdit_code_account)
            config.set(c, 'lineEdit_code_password',lineEdit_code_password)
            config.set(c, 'lineEdit_code_project_id',lineEdit_code_project_id)
            config.set(c, 'checkBox_code_pass_fake_phone',checkBox_code_pass_fake_phone)
        for k,v in operation_label_dict.items():
            config.set("operation", k, str(v))
        config.set("ip", 'lineEdit_ip',self.lineEdit_ip.text())
        with open('./config.ini', "w", encoding='utf-8') as f:
            config.write(f)


    @pyqtSlot()
    #pushButton_stop
    def on_pushButton_stop_clicked(self):
        global stop_flag
        stop_flag = False
        self.textBrowser_log.append("------已经收到暂停操作信号， 等待当前手机号操作结束后， 停止！！！！" )
    # 开始操作的按钮
    @pyqtSlot()
    def on_pushButton_start_clicked(self):
        global  stop_flag
        stop_flag = True
        logger.warning('-------%s', stop_flag)
        if self.auto_phone is None:
            self.judge_information("需要先进行接码配置")
            return
        if not self.lineEdit_ip.text().strip():
            self.judge_information("需要先进行ip配置")
            return
        config_data = {
            'cf': self.cf,
            'lineEdit_certno_count': int(self.lineEdit_certno_count.text().strip()),
            "is_change_phone": self.change_phone.isChecked(),
            "tableWidget": self.tableWidget,
            "checkbox_register_find_password": self.checkBox_register_find_password.isChecked(),
            "lineEdit_register_count": int(self.lineEdit_register_count.text().strip()),
            'lineEdit_thread_count':int(self.lineEdit_thread_count.text().strip()),
            "lineEdit_register_password": self.lineEdit_register_password.text().strip(),
            "lineEdit_login_count": int(self.lineEdit_login_count.text().strip()),
            "lineEdit_login_sleep": self.lineEdit_login_sleep.text().strip(),
            "lineEdit_code_retry": self.lineEdit_code_retry.text().strip(),
            "lineEdit_shiming_retry": self.lineEdit_shiming_retry.text().strip(),
            "lineEdit_ip_err_retry": self.lineEdit_ip_err_retry.text().strip(),
            'lineEdit_ip':self.lineEdit_ip.text().strip(),
            "lineEdit_image_code_err_retry": self.lineEdit_image_code_err_retry.text().strip(),
            "lineEdit_data_use_count": self.lineEdit_data_use_count.text().strip(),
            "tableWidgetrowPosition": self.tableWidget.rowCount(),
        }
        self.save_init()
        self.spider_thread = WeiD(self.auto_phone, self.code_project_id, config_data, password=self.lineEdit_register_password.text().strip())
        self.spider_thread.trigger.connect(self.text_logger)
        self.spider_thread.start()
        
    # 刷新本地资料的按钮
    @pyqtSlot()
    def on_pushButton_refresh_clicked(self):
        self.read_config()
        
    #接码选择按钮
    def comboBox_platform_change(self, text):
       for code in ['code', 'code2', 'code3']:
           now_text = self.cf.get(code, "combobox_platform")
           if text == now_text:
               now_account = self.cf.get(code, "lineEdit_code_account")
               now_code_password = self.cf.get(code, "lineEdit_code_password")
               now_code_project_id = self.cf.get(code, "lineEdit_code_project_id")
               now_code_pass_fake_phone = self.cf.getboolean(code, "checkBox_code_pass_fake_phone")
  
       self.lineEdit_code_account.setText(str(now_account))
       self.lineEdit_code_password.setText(str(now_code_password))
       self.lineEdit_code_project_id.setText(str(now_code_project_id))
       self.checkBox_code_pass_fake_phone.setChecked(now_code_pass_fake_phone)
       
    # 接码登录的按钮
    @pyqtSlot()
    def on_pushButton_code_login_clicked(self):
        category = self.comboBox_platform.currentText()
        code_account = self.lineEdit_code_account.text().strip()
        code_password = self.lineEdit_code_password.text().strip()
        self.code_project_id = self.lineEdit_code_project_id.text().strip()
        self.auto_phone = collectSms(code_account, code_password, category)
        if self.auto_phone.token:
            self.textBrowser_code.append(f"{category}--接码登录成功, 所剩余额： {self.auto_phone.money}")
        else:
            self.textBrowser_code.append(f"{category}--接码登录失败，检查账号密码")

    #ip测试的按钮
    @pyqtSlot()
    def on_pushButton_ip_test_clicked(self):
        url_ip = self.lineEdit_ip.text().strip()
        web = requests.get(url_ip)
        self.label_ip_hidden.setText(web.text)

    def judge_information(self, message, status=True):
        if status:
            QMessageBox.information(self, "信息", message, QMessageBox.Yes, QMessageBox.Yes)
            return
        QMessageBox.information(self, "信息错误", message, QMessageBox.Yes, QMessageBox.Yes)
        return
    def text_logger(self, message):
    
        if message.get("kind") == 'message':
            rowPosition = self.tableWidget.rowCount()
            # count = message['data'].get('count', 1)
            prefix = f"{time.strftime('%H:%M:%S', time.localtime())} {rowPosition} "
            self.textBrowser_log.append(prefix + message["data"])
        elif message.get('kind') == 'delete':
            count = message['data'].get('count', 1)
            self.tableWidget.removeRow(count-1)
        elif message.get("kind") == 'warn':
            self.textBrowser_log.append(message["data"])
        elif message.get("kind") == 'change':
            count = message['data'].get('count', 1)
            # print("收到的message", message)
            phone = message['data'].get('phone')
            rowPosition = self.tableWidget.rowCount()
            print(count, '----------', rowPosition)
            if rowPosition + 1 == count:
                self.tableWidget.insertRow(rowPosition)
                password = self.lineEdit_register_password.text().strip()
                self.tableWidget.setItem(rowPosition, 0, QTableWidgetItem(str(count)))
                self.tableWidget.setItem(rowPosition, 1, QTableWidgetItem(phone))
                self.tableWidget.setItem(rowPosition, 2, QTableWidgetItem(password))
                self.tableWidget.setItem(rowPosition, 3, QTableWidgetItem(""))
            else:
                status = message['data'].get('status', "")
                
                self.tableWidget.item(rowPosition - 1, 3).setText(status)
                if phone:
                    self.tableWidget.item(rowPosition - 1, 1).setText(phone)


class WeiD(QtCore.QThread):
    trigger = QtCore.pyqtSignal(dict)
    def get_content(self):
        mc = get_random_mc()
        cuid = ''.join([random.choice(self.pwd_str) for i in range(32)])
        android_id = ''.join([random.choice(self.pwd_str) for i in range(16)])
        content = '{"alt":"","android_id":"%s","app_status":"active","appid":"com.koudai.weidian.buyer","appv":"5.9.7","brand":"google","build":"20200408164901","channel":"1013n","cuid":"%s","disk_capacity":"16.60GB/24.32GB","feature":"E|F,H|F,P|F,R|T","h":"1794","iccid":"","imei":"","imsi":"","lat":"","lon":"","mac":"%s","machine_model":"armv8l","memory":"1696M/3765M","mid":"Pixel","mobile_station":"0","net_subtype":"0_","network":"WIFI","oaid":"","os":"29","platform":"android","serial_num":"unknown","suid":"%s","w":"1080","wmac":"02:00:00:00:00:00","wssid":"<unknown ssid>"}'%(android_id,cuid, mc, cuid )
        return content
    def __init__(self, auto_phone, project_id, config_data, password='qwe123456', nKey="6cec4a8b268b8749dfa0bd409effcd08"):
        super(WeiD, self).__init__()
        self.lineEdit_certno_count = config_data['lineEdit_certno_count']
        self.is_change_phone = config_data['is_change_phone']
        self.icard_list = self.get_certNo()
        self.nKey = nKey
        self.auto_phone = auto_phone
        self.file_name_list = self.get_img_list()
        self.cf = config_data['cf']
        self.lineEdit_shiming_retry = config_data['lineEdit_shiming_retry']
        self.change_count_limit = config_data['lineEdit_data_use_count']
        self.project_id = project_id
        self.config_data = config_data
        self.tableWidget = config_data['tableWidget']
        self.virtual_catrgory = config_data['lineEdit_image_code_err_retry']
        self.login_sleep_time = config_data['lineEdit_login_sleep']
        self.login_count = config_data['lineEdit_login_count']
        self.checkbox_register_find_password = config_data['checkbox_register_find_password']
        self.ip_url = config_data['lineEdit_ip']
        self.pwd_str = ['1','2', '7', '0', '3', '4', '8', '5', 'q', 'r', 'e', 'h', 'n']
        self.captcha_id = "2003473469"
        self.count = 1
        self.password = password
        self.new_passwd = ''
        self.content = self.get_content()
        self.headers = {
            "Host": "thor.weidian.com",
            "x-schema": "https",
            "referrer": "https://android.weidian.com",
            "referer": "https://android.weidian.com",
            "origin": "android.weidian.com",
            "x-origin": "thor",
            "x-encrypt": "1",
            "user-agent": "Android/10 WDAPP(WDBuyer/5.9.7) Thor/2.3.3",
            "content-type": "application/x-www-form-urlencoded; charset=utf-8",
            "accept-encoding": "GLZip",  # 影响到结果是否解压
        }
        self.ocr = ddddocr.DdddOcr()


    def get_img_list(self):
        file_name_list = os.listdir('.\header_img')
        return  file_name_list

    @staticmethod
    def gzip_compress(buf):
        if isinstance(buf, dict):
            buf = json.dumps(buf, separators=(",", ":"))
        out = io.BytesIO()
        buf = buf.encode()
        with gzip.GzipFile(fileobj=out, mode="w") as f:
            f.write(buf)
        return out.getvalue()

    @staticmethod
    def get_sign(data, salt="2a7d5d1d3c0a4df08bfbacd58fb0748d") -> str:
        data_sort = sorted(data, key=lambda d: d[0])
        sign_str = ""
        for k in data_sort:
            sign_str += k + "=" + data[k] + "&"
        sign_str += salt
        sign_1 = md5(sign_str.encode()).hexdigest().upper()
        sign_2 = md5((sign_1 + salt).encode()).hexdigest().upper()
        return sign_2

    @staticmethod
    def get_sign_buyerPassword(password):
        str1 = base64.b64encode(password.encode()).decode()
        i = 0
        s = ""
        while i < len(str1):
            s += chr(ord(str1[i]) + (i % 3))
            i += 1
        s = s + "koudai_gouwu_is_success_for_ever"
        return md5(s.encode()).hexdigest()

    @staticmethod
    def get_sign_sellerPassword(password):
        s = "_Wedian&&Is##Wonderful**@~0_" + password
        return md5(s.encode()).hexdigest()

    def get_aes_key(self):
        v6 = self.nKey
        v10 = v6[1:]
        v10_index = 0
        v8 = len(v6)
        v11 = v8 >> 1
        aesKey = []
        while v11 > 0:
            if v10_index == 0:
                v13 = ord(v6[v10_index])
            else:
                v13 = ord(v10[v10_index - 1])
            v14 = ord(v10[v10_index])
            v10_index += 2
            if v13 > 0x39:
                v13 = v13 + 9
            if v14 > 0x39:
                v14 = v14 + 9
            v11 = v11 - 1
            aesKey.append(hex((v14 & 0xf | v13 << 4) & 0xff))
        return ''.join(x[2:].zfill(2) for x in aesKey)

    def aes_decrypt_decompress(self, gzip_data, key=None) -> str:
        if key is None:
            key = self.get_aes_key()
        key = bytearray.fromhex(key)
        padtext = gzip_data
        # padtext = pad(gzip_data, 16, style='pkcs7')
        aes = AES.new(key, AES.MODE_ECB)
        message = aes.decrypt(padtext).rstrip()
        # print(message)
        # print(message.split(b"\x00\x00\x00"))
        try:
            message1 = b"\x00\x00\x00".join(message.split(b"\x00\x00\x00")[:-1]) + b"\x00\x00\x00"
            return gzip.decompress(message1).decode()
        except:
            message2 = b"\x00\x00".join(message.split(b"\x00\x00")[:-1]) + b"\x00\x00"
            return gzip.decompress(message2).decode()

    def aes_encrypt_compress(self, message, key=None):
        gzip_data = self.gzip_compress(message)
        if key is None:
            key = self.get_aes_key()
        key = bytearray.fromhex(key)
        padtext = pad(gzip_data, 16, style='pkcs7')
        aes = AES.new(key, AES.MODE_ECB)
        message = aes.encrypt(padtext)
        base64_encrypted = base64.b64encode(message)
        return base64_encrypted.decode()

    def get_encrypt_data(self, param_plain: dict) -> dict:
        param = self.aes_encrypt_compress(json.dumps(param_plain, separators=(",", ":")))
        if isinstance(self.content, dict):
            context_plant = json.dumps(self.content, separators=(",", ":"))
        else:
            context_plant = self.content
        context = self.aes_encrypt_compress(context_plant)
        timestamp = str(int(time.time() * 1e3))
        data = {
            "param": param,
            "v": "2.0",
            "context": context,
            "appkey": "06475281",
            "timestamp": timestamp,
        }
        sign = self.get_sign(data)
        data["sign"] = sign
        return data

    def fetch_vcode(self, param_plain: dict, register_url=None) -> dict:
        #更新代理
        # https://thor.weidian.com/passport/get.vcode/1.0
        url = "https://thor.weidian.com/passport/get.vcode/1.0" if not register_url else register_url
        data = self.get_encrypt_data(param_plain)
        for i in range(3):
            try:
                response = self.myrequests(url=url, method="POST",  headers=self.headers, data=data)
                break
            except Exception as e:
                print(e)
                pass
        else:
            print("重试请求失败e=")
            return
        response_message = self.aes_decrypt_decompress(response.content)
        # self.trigger.emit({'data': f"{response_message}", "kind": "message"})
        print(f"{response_message}")
        return json.loads(response_message)

    def fetch_piccaptcha(self, param_plain):
        url = "https://thor.weidian.com/passport/get.piccaptcha/1.0"
        data = self.get_encrypt_data(param_plain)
        response = self.myrequests(url, method="POST",  headers=self.headers, data=data)
        response_message = self.aes_decrypt_decompress(response.content)
        # print(f"{response_message=}")
        return json.loads(response_message)
    def test_login(self, pwd):
        buyerPassword = self.get_sign_buyerPassword(pwd)
        sellerPassword = self.get_sign_sellerPassword(pwd)
        login_data = {"buyerPassword":f"{buyerPassword}","countryCode":"86","phone":self.phone,"sellerPassword":f"{sellerPassword}"}
        login_url = 'https://thor.weidian.com/passport/login.mobile/1.0'
        login_res = self.fetch_vcode(login_data, login_url)
        cookie = self.deal_success_res(login_res, pwd)
        
        if cookie == '登录出现风控':
            return False
        else:
            return cookie
    def login(self, pwd):
        logger.info('--phone: %s----pwd: %s-开始的登录----', self.phone, pwd)
        buyerPassword = self.get_sign_buyerPassword(pwd)
        sellerPassword = self.get_sign_sellerPassword(pwd)
        login_data = {"buyerPassword":f"{buyerPassword}","countryCode":"86","phone":self.phone,"sellerPassword":f"{sellerPassword}"}
        login_url = 'https://thor.weidian.com/passport/login.mobile/1.0'
        login_res = self.fetch_vcode(login_data, login_url)
        cookie = self.deal_success_res(login_res, pwd)
        return cookie

    def get_certNo(self):
        with open('id_card.txt', 'r', encoding='utf-8') as f:
            result = f.readlines()
        idcard_list = []
        for ic in result:
            now_icar = {}
            icard_info = ic.strip().split('----')
        
            id_card_flag = icard_info[2] if len(icard_info)>2 else 0
            
            now_icar['certNo'], now_icar['userName'], flag  = icard_info[1], icard_info[0], id_card_flag
            if int(id_card_flag)>=0 and int(id_card_flag)< self.lineEdit_certno_count:
                idcard_list.append(now_icar)
        return idcard_list
    def update_certno(self, cerNo, info):
        f = open('id_card.txt','r', encoding='utf-8')
        a = f.readlines()
        f = open('id_card.txt','w', encoding='utf-8')
        for i in a:
            if cerNo in i:
                content_list = i.split('----')
                if len(content_list)>2:
                    if info==-1:
                        write_content = '----'.join(content_list[0:2]).strip()+'----'+'-1' +'\n'
                    else:
                        now_number = int(content_list[-1]) + 1
                        write_content = '----'.join(content_list[0:2]).strip()+'----'+f'{now_number}' +'\n'
                
                else:
                    write_content = i.strip() +f'----{info}'  +'\n'
                f.write(write_content)
            else:
                f.write(i)
        
        f.close()
    def authentication(self, cookie, pwd):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 9; MI 8 UD Build/PKQ1.180729.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/102.0.5005.125 Mobile Safari/537.36  WDAPP(WDBuyer/5.9.7) appid/com.koudai.weidian.buyer KDJSBridge2/1.1.0',
            'Cookie': cookie,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Referer': 'https://weidian.com/',
            'X-Requested-With': 'com.koudai.weidian.buyer'
        
        }
        fial_time = 0
        def authen():
            self.icard_list = self.get_certNo()
            global fial_time
            timestamp = str(int(time.time() * 1e3))
            get_no = 'https://thor.weidian.com/authentication/realname.createAuthOrder/1.0'
            get_data = {'v': '1.0',
                        'timestamp': timestamp}
            res_no = self.myrequests(url=get_no, method="POST",  data=get_data, headers=headers)
            logger.info(res_no.text)
            if 'LOGIN ERROR' in res_no.text:
                logger.warning('---------需要重新登录-phone: %s----', self.phone)
                cookie = self.login(pwd)
                headers['Cookie'] = cookie
            elif '已完成实名认证请勿重复认证' in res_no.text:
                logger.info('%s---->此账号已经实名------------', self.phone)
                return {'certNo': '默认实名', 'userName': '默认实名'}
        
            requestNo = json.loads(res_no.text)['result']['requestNo']
        
        
            now_idcard = self.icard_list.pop()
            certNo, userName  = now_idcard.get('certNo'), now_idcard.get('userName')
            url = 'https://thor.weidian.com/authentication/realname.submitAuthOrder/1.0'
            data = {"param":str({"certType":"IC","certNo":f"{certNo}","userName":f"{userName}","phone":"","bankNo":"","cvv":"","ValidDate":"","frontImg":"","handImg":"","authSource":"h5","authType":"iclevel2","requestNo":f"{requestNo}"}),
                    'v': '1.0',
                    'timestamp':    timestamp
            
                    }
            logger.info(data)
            res = self.myrequests(url=url,method="POST", headers=headers, data=data)
            logger.info(res.content.decode())
        
            if '认证失败' in res.text:
                fial_time+=1
                fail_card = f'{userName}----{certNo}----failed\n'
                self.save_data2text(fail_card,'fail_card.txt')
                logger.warning('-------当前身份证不可用， 开始更换身份证-----------')
                self.update_certno(certNo, -1)
                time.sleep(1)
                if fial_time>10:
                    return False
                else:
                    return authen()
            elif '当前证件绑定的店铺数已达上限' in res.text:
                logger.warning('-------当前证件绑定的店铺数已达上限-----------')
                self.update_certno(certNo, 1)
                return authen()
            elif 'LOGIN ERROR' in res.text:
                logger.warning('---------需要重新登录-phone: %s----', self.phone)
                cookie = self.login(pwd)
                headers['Cookie'] = cookie
                return authen()
            elif '该账号存在风险，您的申请不予通过' in res.text:
                logger.warning('%s---->经平台安全系统检测，该账号存在风险，您的申请不予通过----------', self.phone)
            elif '实名认证次数超限,请在1小时后重试' in res_no.text:
                logger.warning('%s---->实名认证次数超限,请在1小时后重试----------', self.phone)
                return False
            else:
                logger.warning(f'-----实名成功的返回: {res_no.text}---')
                self.trigger.emit({'data': f"实名成功 phone: {self.phone}, 身份证:{certNo}----名字为：{userName}", "kind": "message"})
                self.update_certno(certNo, 1)
                return now_idcard
        return authen()
    def midway_authentication(self, phone):
        self.trigger.emit({'data': f"phone: {self.phone}开始进行实名认证", "kind": "message"})
        headers = {
            'Accept': 'application/json, */*',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 9; MI 8 Lite Build/PKQ1.181007.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobile Safari/537.36  WDAPP(WDBuyer/5.9.7) appid/com.koudai.weidian.buyer KDJSBridge2/1.1.0',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'X-Requested-With': 'com.koudai.weidian.buyer',
            'Referer': 'https://sso.weidian.com/login/modify/index.php?wfr=profile',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
            
            
        }
        slid_flag = False
        for i in range(5):
            url = 'https://thor.weidian.com/jupiter/sendSMS/1.0?param='
            data = '{"countryCode":"86","telephone":"%s","authType":"SLIDEIMAGE","isTrusted":true}'%phone
            
            now_url  = url + parse.quote(data)
        
            res = self.myrequests(url=now_url, method='GET', headers=headers)
            appId = res.json()['result']
            '''{"status":{"code":200900,"message":"前置滑动验证码","description":""},"result":"2003473469"}'''
            randStr, ticket, = self.get_ticket(appId)
            if randStr:
                slid_flag = True
                self.trigger.emit({'data': f"phone: {self.phone}顺利通过滑块", "kind": "message"})
                break
            else:
                self.trigger.emit({'data': f"phone: {self.phone}滑块失败{i+1}次", "kind": "message"})
        if slid_flag:
            url2 = 'https://thor.weidian.com/jupiter/sendSMS/1.0?param='
            data2 = '{"countryCode":"86","telephone":"%s","authType":"SLIDEIMAGE","ticket":"%s","appid":"%s","randstr":"%s","appId":"%s","randStr":"%s"}'%(phone, ticket, appId, randStr, appId, randStr)
            send_code_url = url2 + parse.quote(data2)
            '''{"status":{"code":0,"message":"成功","description":""},"result":null}'''
            category = self.auto_phone.category
            user_name = self.auto_phone.user_name
            passwd = self.auto_phone.passwd
            testphone = collectSms( user_name, passwd, category)
            #"10375----2358Q0"
            testphone.release_phone(self.project_id, phone)
            testphone.get_phone_number(self.project_id, phone)

            time.sleep(3)
            res2 = self.myrequests(url=send_code_url, method='GET', headers=headers)

            if res2.json()['status']['message'] == '成功':
                self.trigger.emit({'data': f"phone: {self.phone} 正在获取短信验证码， 请等待", "kind": "message"})
                
                sms_code = testphone.test_send_code(self.project_id, phone)
            
                if sms_code:
                    for g in range(6):
                        self.icard_list = self.get_certNo()
                        now_idcard = self.icard_list.pop()
                        certNo, userName  = now_idcard.get('certNo'), now_idcard.get('userName')
                        submit_url = 'https://thor.weidian.com/jupiter/certifyIdentityNoName/1.0'
                        submit_data =  {'param': str({"identityNo":"{}".format(certNo),"name":"{}".format(userName),"countryCode":"86","telephone":"{}".format(phone),"verifyCode":"{}".format(sms_code),"authType":"SLIDEIMAGE"})}
                        
                        res3 = self.myrequests(url=submit_url, method='POST', headers=headers, data=submit_data)
                        print(res3.text)
                    
                        if res3.json()['status']['code'] == 0:
                            self.trigger.emit({'data': f"phone: {self.phone}实名成功， 开始重新登录", "kind": "message"})
                            self.update_certno(certNo, 1)
                            return True
                        elif '当前证件绑定的店铺数已达上限' in res3.text:
                            self.trigger.emit({'data': f"phone: {self.phone}，此{certNo}当前证件绑定的店铺数已达上限", "kind": "message"})
                            self.update_certno(certNo, 1)

                        elif '认证失败' in res3.text:
                            self.trigger.emit({'data': f"phone: {self.phone}，此{certNo}身份证无法绑定", "kind": "message"})
                            self.update_certno(certNo, -1)
                        else:
                            time.sleep(2)
                            self.trigger.emit({'data': f"phone: {self.phone}绑定身份证失败{g+1}次, message: {res3.text}， ", "kind": "message"})
                            continue
                else:
                    self.trigger.emit({'data': f"phone: {self.phone}实名过程中，平台未获取到短信验证码而是失败", "kind": "message"})
            else:
                self.trigger.emit({'data': f"phone: {self.phone}实名验证中， 触发短信验证码失败", "kind": "message"})
                return False
        else:
            self.trigger.emit({'data': f"phone: {self.phone}滑块验证都失败了， 放弃操作", "kind": "message"})
            return False

    def change_pwd(self):
        change_url = 'https://thor.weidian.com/passport/modify.passwd/1.0'
    
        data1 = {"countryCode": "86", "phone": self.phone}
        res = self.fetch_vcode(data1)
        if res["status"]["code"] == 430001 and res["status"]["description"] == "请填写图中验证码":
            session_id = res["status"]["message"]
            change_flag = False
            for i in range(10):
                param_verify = {'sessionId': session_id}
                verify_res = self.fetch_piccaptcha(param_verify)
                pictureB64 = verify_res['result']['picture']
                v_code = self.ocr.classification(img_base64=pictureB64)
                send_data ={"captchaAnswer":f"{v_code}","captchaSession":f"{session_id}","countryCode":"86","phone":f"{self.phone}"}
                send_res = self.fetch_vcode(send_data)
                if send_res['status']['code'] == 430002:
                    self.trigger.emit({'data': f"更改密码时验证码输入错误,message={send_res['status']['description']}, 重试次数:{i}", "kind": "message"})
                    time.sleep(1)
                    continue
                elif send_res['status']['code'] == 420012:
                    self.trigger.emit({'data': f"触发短信验证码错误message={send_res['status']['description']}, ", "kind": "message"})
                    return  'fail'
                else:
                    change_flag = True
                    self.trigger.emit({'data': {"status": "正在接收短信验证码 ", "count": self.count, 'phone': self.phone}, "kind": "change"})
                    break
            if not change_flag:
                self.trigger.emit({'data': {"status": "图像验证码识别错误，终止操作", "count": self.count, 'phone': self.phone}, "kind": "change"})
                return 'fail'
            change_code = self.get_sms_code()
            if change_code:
                self.trigger.emit({'data': {"status": f"收到短信验证码{change_code} ", "count": self.count, 'phone': self.phone}, "kind": "change"})
                # new_pawd = self.password+ ''.join([choice(self.pwd_str) for _ in range(3)])
                new_pawd = self.password
                self.new_passwd = new_pawd

                logger.info('%s---->更改密码为：%s-------',self.phone, new_pawd)
                buyerPassword = self.get_sign_buyerPassword(new_pawd)
                sellerPassword = self.get_sign_sellerPassword(new_pawd)
                change_data = {"action":"login","buyerPassword":f"{buyerPassword}","countryCode":"86","phone":f"{self.phone}","sellerPassword":f"{sellerPassword}","vcode":f"{change_code}"}
                change_res = self.fetch_vcode(change_data, change_url)
                change_flag = self.deal_success_res(change_res, new_pawd)
                save_data = self.phone + '----'+ new_pawd +'\n'
                self.save_data2text(save_data, '注册成功账号.txt')
            else:
                self.deal_phone()
                self.trigger.emit({'data': {"status": "失败,平台未获取到短信验证码 ", "count": self.count,  'phone': self.phone}, "kind": "change"})

                self.trigger.emit({'data': f"{self.phone}---->更改密码失败---因为获取短信验证码失败", "kind": "message"})
                error_info = '%s---->更改密码失败---因为获取短信验证码失败' % self.phone
                print(error_info)
                self.save_data2text(error_info + '\n', '失败的记录.txt')
                return 'fail'
    
        else:
            self.trigger.emit({'data': {"status": "图像验证码识别都失败,终止后续操作 ", "count": self.count, 'phone': self.phone}, "kind": "change"})

            self.trigger.emit({'data': f"{self.phone}---->更改重试5次图像验证码都失败，退出", "kind": "message"})

            error_info = "%s---->更改重试5次图像验证码都失败，退出" % self.phone
            print(error_info)
            self.save_data2text(error_info+ '\n', '失败的记录.txt')
            #释放手机号
            if project_debug:
                return 'fail'
            self.auto_phone.release_phone(self.project_id, self.phone)
            return 'fail'

    def save_data2text(self, data, file_name):
        with open(file_name, 'a', encoding='utf-8') as  f:
            f.write(data)
    def deal_success_res(self, res, password):
        status_code = res['status']['code']
        if res['status']['message'] == 'OK' or  status_code in [420064, 50, 420009]:
        
            if status_code == 420064:
                self.trigger.emit({'data': f"phone: {self.phone}登录出现风控-->根据《网络安全法》等法律法规要求", "kind": "message"})

                error_info = '%s---->登录出现风控---------'%self.phone
                logger.warning(error_info)
                self.save_data2text(error_info+'\n', '失败的记录.txt')
                self.deal_phone()
                return '登录出现风控'
            elif status_code == 420009:
                return '该手机号未注册'
            elif status_code == 50:
                redirect_url = res['result']['redirect']
                print(redirect_url)
                logger.warning('------需要短信方式验证 phone: %s-----------', self.phone)
                if '请实名认证后安全使用微店' in res['status']['description']:
                    self.trigger.emit({'data': {"status": "需要实名验证 ", "count": self.count, 'phone': self.phone}, "kind": "change"})
                    auth_flag = self.midway_authentication(self.phone)
                    if auth_flag:
                        return self.login(password)
                    else:
                        return
                else:
                    v_f = self.verify_phone(redirect_url)
                #验证码通过， 开始重新登录
                if v_f:
                    return self.login(password)
                else:
                    logger.warning('-----手机短信验证码失败----')
                    return
            else:
                self.trigger.emit({'data': f"账号登录或者注册成功 phone: {self.phone}, 密码:{password}", "kind": "message"})
                # save_data = self.phone + '----'+ password +'\n'
                # self.save_data2text(save_data, '注册成功账号.txt')
            result = res['result']
            uid = result['uid']
            cookie_dict = {c.get('name'): c.get('value') for c in  result['cookie']}
            cookie = ';'.join([k+'='+v for k, v in  cookie_dict.items()])
            now_cookie = cookie + ';' + f'uid={uid}'
            return now_cookie

            #开始绑定身份证
            # auth_flag = self.authentication(now_cookie, password)
            # if type(auth_flag) == dict:
            #     logger.info('--------身份证绑定成功, 开始释放，拉黑手机号:%s'%self.phone)
            #     self.deal_phone()
            #     data = f'{self.phone}----{password}----{auth_flag.get("userName")}----{auth_flag.get("certNo")}\n'
            #     self.save_data2text(data, '实名成功账号.txt')
            #     self.upload_headers(now_cookie)
            #     return True
            # else:
            #     self.deal_phone()
            #     error_info = '%s----%s--绑定身份证出现风控-----'% (self.phone, password)
            #     logger.warning(error_info)
            #     self.save_data2text(error_info, '失败的记录.txt')


    def deal_captcha(self, session_id, count):
        flag = False
        for i in range(1, int(self.config_data['lineEdit_code_retry']) + 1):
            self.trigger.emit({'data': {"status": f"第[{i}]次填写验证码", "count": self.count, 'phone': self.phone}, "kind": "change"})
            param_verify = {'sessionId': session_id}
            verify_res = self.fetch_piccaptcha(param_verify)
            pictureB64 = verify_res['result']['picture']
            captchaSession = verify_res['result']['sessionId']
            v_code = self.ocr.classification(img_base64=pictureB64)
            send_data = {"action": "register", "captchaAnswer": f"{v_code}", "captchaSession": f"{captchaSession}", "countryCode": "86", "phone": self.phone}
            send_res = self.fetch_vcode(send_data)
            if send_res is None:
                self.trigger.emit({'data': {"status": f"第[{i}]次获取验证码失败", "count": self.count, 'phone': self.phone}, "kind": "change"})
                time.sleep(1)
                continue
            if send_res['status']['code'] == 430002:
                self.trigger.emit({'data': {"status": f"重试验证码[{i}]次", "count": self.count, 'phone': self.phone}, "kind": "change"})
                self.trigger.emit({'data': f"{send_res['status']['description']}, 重试次数:{i}", "kind": "message"})
                time.sleep(1)
                continue
            else:
                flag = True
                break
            # elif send_res['status']['code'] == 420008:
            #     self.trigger.emit({'data': {"status": f"注册失败，已经注册", "count": count}, "kind": "change"})
            #     self.trigger.emit({'data': f"---------该手机号： {self.phone}已经注册----", "kind": "message"})
            #     with open('already_register_phone.txt', 'a+', encoding='utf-8') as f:
            #         f.writelines(self.phone + '\n')
            #     self.deal_phone()
            #     return False
        
        if not flag:
            self.trigger.emit({'data': {"status": f"验证码识别全部错误,已经终止后续操作 ", "count": self.count, 'phone': self.phone}, "kind": "change"})
            self.trigger.emit({'data': f"图像验证码失败都失败，此号{self.phone}退出", "kind": "message"})
            self.auto_phone.release_phone(self.project_id, self.phone)
            return False
        return True

    def start_register(self, special_phone=None):
        self.ip_url = self.config_data["lineEdit_ip"]
        change_succes_number = 0
        register_number = int(self.config_data["lineEdit_register_count"])
        tableWidgetrowPosition = int(self.config_data["tableWidgetrowPosition"])
        delete_number = 0
        MAX_NUMBER = 2 if special_phone else 1000000
        for count in range(1, MAX_NUMBER):
            try:
                time.sleep(1)
                if change_succes_number == register_number or not stop_flag:
                    break
                try:
                    # abs_count = 0   if count<register_number+1 else  count - register_number
                    self.count = count + tableWidgetrowPosition - delete_number
                    count = count + tableWidgetrowPosition - delete_number
                    self.content = self.get_content()
                    res_json = self.auto_phone.get_phone_number(self.project_id, phone_number=special_phone)
                    if res_json.get("message", "") == "目前没有满足要求的卡号" and not res_json.get("data"):
                        self.trigger.emit({'data': json.dumps(res_json, ensure_ascii=False), "kind": "warn"})
                        return
                    try:
                        phone = res_json['mobile']
                        int(phone)
                    except:
                        self.trigger.emit({'data':  f"此{self.auto_phone.category}项目id：{self.project_id}没有获取到手机号", "kind": "warn"})
                        return
                    self.phone = phone
                    self.trigger.emit({'data': {"phone": phone, "count": count}, "kind": "change"})
                    if self.phone_is_registed(phone):
                        if self.checkbox_register_find_password:
                            if self.test_login(self.password):
                                
                                self.trigger.emit({'data': {"status":'正在更改密码', "count": count, 'phone': self.phone}, "kind": "change"})
                                
                                chang_flag = self.change_pwd()
                                if chang_flag == 'fail':
                                    continue
                            else:
                                self.trigger.emit({'data': {"status":'账号已经被封，不进行改密操作', "count": count, 'phone': self.phone}, "kind": "change"})
                                self.deal_phone()
                                continue
                        else:
                            self.trigger.emit({'data': {"status":'账号已经注册了，不进行改密操作', "count": count, 'phone': self.phone}, "kind": "change"})
                            self.trigger.emit({'data': {"status":'', "count": count, 'phone': self.phone}, "kind": "delete"})
                            delete_number +=1
                            self.deal_phone()
                            continue
                    else:
                        param_plain = {"action": "register", "countryCode": "86", "phone": phone}
                        res = self.fetch_vcode(param_plain)
                        if res is None:
                            self.trigger.emit({'data': {"status": f"注册失败,请求失败", "count": count, 'phone': self.phone}, "kind": "change"})
                        elif res["status"]["code"] == 420012:
                            self.trigger.emit({'data': f"{phone}----此次注册失败--验证码请求次数超限", "kind": "message"})
                            
                            continue
                        elif res["status"]["code"] == 430001 and res["status"]["description"] == "请填写图中验证码":
                            session_id = res["status"]["message"]
                            flag = self.deal_captcha(session_id, count)
                            if not flag:
                                continue
                            sms_code = self.get_sms_code()
                            if sms_code:
                                buyerPassword = self.get_sign_buyerPassword(self.password)
                                sellerPassword = self.get_sign_sellerPassword(self.password)
                                register_url = 'https://thor.weidian.com/passport/reg.person/1.0'
                                register_data = {"buyerPassword": f"{buyerPassword}", "countryCode": "86", "phone": phone, "sellerPassword": f"{sellerPassword}", "vcode": f"{sms_code}"}
                                register_res = self.fetch_vcode(register_data, register_url)
                                result_flag = self.deal_success_res(register_res, self.password)
                                save_data = self.phone + '----'+ self.password +'\n'
                                self.save_data2text(save_data, '注册成功账号.txt')
                            else:
                                self.deal_phone()
                                self.trigger.emit({'data': {"status": f"注册失败,平台未获取到短信验证码", "count": count, 'phone': self.phone}, "kind": "change"})
                                self.trigger.emit({'data': f"{phone}----此次注册失败---因为平台未获取到短信验证码", "kind": "message"})
                                continue
                        elif res['status']['code'] == 420008:
                            logger.info('---------该手机号： %s已经注册----', self.phone)
                            with open('already_register_phone.txt', 'a+', encoding='utf-8') as f:
                                f.writelines(self.phone+'\n')
                            self.deal_phone()
                            continue
                        else:
                            continue
                    #登录验证一下
                    now_pwd = self.new_passwd if self.new_passwd else self.password
                    savecookie = ''
                    for i in range(int(self.login_count)):
                        time.sleep(int(self.login_sleep_time))
                        self.trigger.emit({'data': {"status": f"开始登录第: {i+1}次", "count": count, 'phone': self.phone}, "kind": "change"})
                        self.trigger.emit({'data': f"----开始登录第: {i+1}次---- ", "kind": "message"})
                        savecookie = self.login(now_pwd)
                        if savecookie == '登录出现风控':
                            savecookie = ''
                            break
                    if savecookie:
                        self.trigger.emit({'data': {"status": "获取cookie成功", "count": count, 'phone': self.phone}, "kind": "change"})
                        self.save_data2text(self.phone+'|'+now_pwd+'|'+savecookie + '}}}'+self.project_id+'\n', '登录成功的cookie记录.txt')
                        change_succes_number+=1
                        self.upload_headers(savecookie)
                    
                    
                    else:
                        error_info = '%s---->获取cookie失败-------pwd: %s --'%(self.phone, now_pwd)
                        self.save_data2text(error_info+'\n', '失败的记录.txt')
                        self.trigger.emit({'data': {"status": "获取cookie失败", "count": count, 'phone': self.phone}, "kind": "change"})
                    self.deal_phone()
                
                except:
                    self.trigger.emit({'data': {"status": "发生错误，终止操作", "count": count, 'phone': self.phone}, "kind": "change"})
                    logger.error('----发生错误： %s-------------',traceback.format_exc() )
                    continue
                count +=1
                logger.warning('------已经成功： %s---------', change_succes_number)
            except:
                self.trigger.emit({'data': {"status":'失败', "count": count, }, "kind": "change"})


        self.trigger.emit({'data': f"-----此次注册结束 成功： {change_succes_number}  ----- ", "kind": "message"})

    def run(self):
        
        if self.is_change_phone:
            self.start_change()
        else:
            self.start_register()

    
    
    def deal_phone(self):
        self.auto_phone.shield_phone(self.project_id, self.phone)
        self.auto_phone.release_phone(self.project_id, self.phone)

    def get_sms_code(self):
        for i in range(5):
            time.sleep(20)
            code = self.auto_phone.get_sms(self.project_id, self.phone)
            if code:
                self.trigger.emit({'data': f"--------获取到短信 phone: {self.phone}---code:{code}--", "kind": "message"})
                return code
            else:
                continue

    def phone_is_registed(self, phone):
        url = 'https://sso.weidian.com/user/bindcheck'

        headers = {
            'content-type': 'application/x-www-form-urlencoded',
            'cookie': 'wdtoken=5d712ee3; __spider__visitorid=d767f2242fa48f02; __spider__sessionid=5b9309e9dc1413a0; vc_token_id=b43bf129-315c-e01e-a1ac-f09d0080b91f-1824ff661e7-3fa9; vc_fpcookie=eda2176d-1847-2b4d-9bf2-717d1210ca9d; vc_device_id=f7bf0a7bc4072e1da999482cded4d4ef',
            'referer': 'https://sso.weidian.com/login/index.php?redirect=https%3A%2F%2Fweidian.com%2Fweidian-h5%2Fuser%2Findex.html%3F',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36'
        }

        data = {

            'phone': phone,
            'countryCode': '86',

        }
        res = self.myrequests(url=url, method="POST", headers=headers, data=data)
        res_json = json.loads(res.text)
        registed = res_json.get('result').get('registed')
        print(res_json)
        if registed == 0:
            self.trigger.emit({'data': f"{phone}: 未注册", "kind": "message"})
            return False
        else:
            # self.trigger.emit({'data': f"{phone}: 已经被人注册过了", "kind": "message"})
            return True
    def mod_name(self, cookie):
        with open('name.txt', 'r', encoding='gbk') as f:
            name_list = f.readlines()
        name_list = [i for i in name_list if i]
        now_name = choice(name_list).strip()
        headers = {
            'cookie': cookie,
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
            'referer': 'https://h5.weidian.com/',
            'accept': 'application/json, */*',
        }
        p_name = parse.quote(now_name)
        res2 =  self.myrequests(url=f'https://thor.weidian.com/passport/modify.info/1.0?param=%7B%22nickname%22%3A%22{p_name}%22%7D&wdtoken=7bff0698&_=1661217586358',method="GET",  headers=headers)
        self.trigger.emit({'data': f"--------修改昵称 phone: {self.phone}---message:{res2.json()['status']['message']}--", "kind": "message"})
        logger.info('%s---->修改昵称----------%s',self.phone, res2.text)
        
    def get_header_url(self, cookie):
        try:
            url = "https://vimg.weidian.com/upload/v3/direct?scope=h5user&fileType=image"
            now_header_img =  choice(self.file_name_list)
            suffix = now_header_img.split('.')[-1]
            fields = {
                'file': (now_header_img, open('.\header_img\\' + now_header_img, "rb"), suffix),
                "wdtoken": "11d93997"
            }
            # 因为16位数随机的，每次都不一样
            boundary = '----WebKitFormBoundary' \
                       + ''.join(random.sample(string.ascii_letters + string.digits, 16))
            m = MultipartEncoder(fields=fields, boundary=boundary)
            headers = {
                'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
                'cookie': cookie,
                "Content-Type": m.content_type
            }
            
            req = self.myrequests(url=url, method="POST", headers=headers,
                                data=m)
            headerUrl = req.json().get('result').get('url')
            return  headerUrl
        except:
            with open('header_url.txt', 'r') as f:
                result = f.readlines()
            return choice(result).strip()
    def upload_headers(self, cookie):
    
        header_url = self.get_header_url(cookie)
        param = {"headurl": header_url}
        param_ = parse.quote(json.dumps(param))
        url = 'https://thor.weidian.com/passport/modify.info/1.0?param={}&wdtoken=71197002&_=1660034906241'.format(param_)
    
        headers = {
            'cookie': cookie,
            'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary1fhbLvX7THlbZP6K',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
            'referer': 'https://h5.weidian.com/'
    
        }
        res2 =  self.myrequests(url=url, method="GET", headers=headers)
        self.trigger.emit({'data': f"--------上传头 phone: {self.phone}---message:{res2.json()['status']['message']}--", "kind": "message"})
        logger.info('%s---->上传头像----------%s',self.phone, res2.text)
        time.sleep(2)
        self.mod_name(cookie)



    def verify_phone(self, vurl):

        sessionId = vurl.split('sessionId=')[-1]
        self.trigger.emit({'data': f"--------开始进行手机短信验证 phone: {self.phone}-- sessionId: {sessionId}---", "kind": "message"})

        url = f'https://sso.weidian.com/login/check_center.html#/?sessionId={sessionId}'
        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 9; MI 8 UD Build/PKQ1.180729.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/102.0.5005.125 Mobile Safari/537.36  WDAPP(WDBuyer/5.9.7) appid/com.koudai.weidian.buyer KDJSBridge2/1.1.0',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Referer': 'https://sso.weidian.com/',
            'X-Requested-With': 'com.koudai.weidian.buyer'
    
        }
        res = self.myrequests(url=url, method='GET', headers=headers)
        cookie_dict = requests.utils.dict_from_cookiejar(res.cookies)
        wdtoken  = cookie_dict.get('wdtoken')
        send_code = 'https://thor.weidian.com/calibrationsdkproxy/vcode.get/1.0'
        data = {'param': str({"sessionId":"%s"%sessionId,"captchaSessionId":"","captchaCode":""}),
    
                'wdtoken':wdtoken }
    
        headers['cookie'] = ';'.join([k+'='+v for k,v in cookie_dict.items()]) + ';vc_token_id=fb52b1fe-56ed-832f-85f5-2d1da8cfa8cc-1829d6f39b2-bd50; vc_fpcookie=69371be6-1899-0ab9-28ad-d3f0040139b9; vc_device_id=4f82a25b72ca5d575d06bb28c506c090'
        headers['content-type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
        category = self.auto_phone.category
        user_name = self.auto_phone.user_name
        passwd = self.auto_phone.passwd
        testphone = collectSms( user_name, passwd, category)
        #"10375----2358Q0"
        testphone.release_phone(self.project_id, self.phone)
        testphone.get_phone_number(self.project_id, self.phone)
    
        time.sleep(3)
        re2 = self.myrequests(url=send_code,method="POST", data=data, headers=headers)
        print(re2.text)
        if re2.json()['status']['message'] == 'OK':
            sms_code = testphone.test_send_code(self.project_id, self.phone)
            submit_code_url = 'https://thor.weidian.com/calibrationsdkproxy/vcode.check/1.0'
            submit_data = {
                'param': str({"code":"%s"%sms_code,"sessionId":"%s"%sessionId}),
                'wdtoken': wdtoken
        
            }
            submit_res = self.myrequests(url=submit_code_url, method="POST", data=submit_data, headers=headers)
            '''{"status":{"code":0,"message":"OK","description":""},"result":{"hasNext":false}}'''
            print(submit_res.text)
            if submit_res.json()['status']['message'] == 'OK':
                self.trigger.emit({'data': f"--------短信验证成功, 开始尝试连续登录 phone: {self.phone}-- sessionId: {sessionId}---", "kind": "message"})

                logger.info('phone：%s短信验证成功', self.phone)
                return True
        else:
            logger.warning('-phone:  %s验证时触发短信失败----------', self.phone)
            return False

    def get_proxy(self):
        res = requests.get(url=self.ip_url)
        try:
            ip_list = res.json()['obj'][0]
            
        except:
            logger.error('---获取代理ip时报错了, 请解决---%s', res.text)
        else:
            proxies_str = f'{ip_list.get("ip")}:{ip_list.get("port")}'
            proxiess = {'https': 'https://%s'%proxies_str, 'http':'https://%s'%proxies_str }
            with open('proxies.txt', 'w') as f:
                f.write(json.dumps(proxiess))
            return True
    def read_proxy(self):
        with open('proxies.txt', 'r') as f:
            proxies = json.loads(f.read())
        return proxies
    def myrequests(self,url, method, data=None, json=None, **kwargs):
     
        for i in range(10):
            proxies = self.read_proxy()
            logger.info('-------当前使用代理: %s----' % proxies)
            try:
                if i >5:
                    proxies = {}
                if method == 'GET':
                    return requests.get(url, proxies=proxies,  timeout=6, **kwargs)
                elif method == "POST":
                    return requests.post(url, data=data, json=json,timeout=6, proxies=proxies,  **kwargs)
                else:
                    logger.error("--------请求类型需要适配")
                    return
            except:
                logger.error('----------当前代理： %s, 不可用。 开始更换代理-- 错误次数： %s', proxies, i)
                self.get_proxy()
        
        
        
    def start_change(self):
        self.conn = sqlite3.connect('real_card.db')
        self.c = self.conn.cursor()
        self.trigger.emit({'data': f"-----此次换绑开始---- ", "kind": "message"})
    
        self.virtual_user_name, self.virtual_passwd = '', ''
        for se in self.cf.sections():
            if se.startswith("code"):
                now_comboBox_platform = self.cf.get(se, "comboBox_platform")
                if now_comboBox_platform == self.virtual_catrgory:
                    self.virtual_project_id = self.cf.get(se, "lineedit_code_project_id")
                    
                    self.virtual_user_name, self.virtual_passwd = self.cf.get(se, "lineedit_code_account"), self.cf.get(se, "lineedit_code_password")
                    
        self.virtual_auto_phone = collectSms(user_name=self.virtual_user_name, passwd=self.virtual_passwd, category=self.virtual_catrgory)
        self.ip_url = self.config_data["lineEdit_ip"]
        succes_number = 0
        register_number = int(self.config_data["lineEdit_register_count"])
        tableWidgetrowPosition = int(self.config_data["tableWidgetrowPosition"])
        delete_number = 0
        with open('换绑实卡账号.txt', 'r', encoding='utf-8') as f:
            cookie_result = f.readlines()

        for data in cookie_result:
            data_list = data.replace('}}}', '|').split('|')
            phone = data_list[0]
            pwd = data_list[1]
            cookie = data_list[2]
            project_id = data_list[3]
            insert_tb_cmd = '''insert or ignore into real_phone_log(phone, pwdd, cookie, project_id) values("%s","%s","%s","%s")'''%(phone, pwd, cookie, project_id)

            self.c.execute(insert_tb_cmd)
            self.conn.commit()
        for count in range(1, 1000):
            try:
                time.sleep(5)
                if succes_number == register_number or not stop_flag:
                    print('----register_number---',register_number,'----succes_number--------------', succes_number)
                    break
                true_info = self.get_real_phone()
                if true_info:
                    count = count + tableWidgetrowPosition - delete_number
                    
                    true_phone, pwd, true_cookie, project_id = true_info[0], true_info[1], true_info[2], true_info[3]
                    
                    flag = self.change_phone(true_phone.strip(), pwd.strip(),  project_id.strip(), true_cookie,  count)
                    if flag:
                        upsql  = '''update real_phone_log set   operation_count = operation_count + 1 where phone="%s"'''%(true_phone)
                        self.c.execute(upsql)
                        self.conn.commit()
                        succes_number+=1
                    else:
                        pass
                
                else:
                    self.trigger.emit({'data': f"-----此次换绑结束,无可用实卡----- ", "kind": "message"})
                    return
            except:
                self.trigger.emit({'data': {"status":'失败', "count": count, }, "kind": "change"})


        self.trigger.emit({'data': f"-----此次换绑结束 成功： {succes_number}  ----- ", "kind": "message"})
        self.c.close()
        self.conn.close()
    def get_real_phone(self):
        
        for count in range(20):
            time.sleep(0.2)
            sql ='''select  phone, pwdd, cookie, project_id, operation_count from real_phone_log where operation_count<%s and is_banned=0 limit 1'''%(int(self.change_count_limit)+1)
            self.c.execute(sql)
            result = self.c.fetchall()
            print(result)
            if not result  or  not stop_flag:
                return False
            nowdata = result[0]
            phone = nowdata[0]
            pwdd = nowdata[1]
            cookie = nowdata[2]
            project_id = nowdata[3]
            operation_count = nowdata[4]
            juge_flag = self.juge_cookie(cookie)
            
            if juge_flag == '该手机号未注册':
                self.trigger.emit({'data': f"-----得到可用实卡：{phone} -- 但是现在未注册， 开始注册-", "kind": "message"})
                self.start_register(phone)
            elif juge_flag:
                self.trigger.emit({'data': f"-----得到可用实卡：  {phone} ---- 已经换绑次数：{operation_count}", "kind": "message"})
                
                return (phone, pwdd, cookie, project_id)
            else:
                self.phone = phone
                self.trigger.emit({'data': f"-----实卡：  {phone} cookie失效： 需要重新登录-- 已经换绑次数：{operation_count}--- ", "kind": "message"})
            
                cookie = self.login(pwdd)
                logger.info(f'--登录后的cookie------->:{cookie}-----------',)
                if cookie in ['登录出现风控']:
                    self.trigger.emit({'data': f"-----实卡{phone}：{cookie},   账号无效, 已经换绑次数：{operation_count}  ----- ", "kind": "message"})
                
                    upsql  = '''update real_phone_log set  is_banned==1  where phone="%s"'''%(phone)
                    self.c.execute(upsql)
                    self.conn.commit()
                elif cookie == '该手机号未注册':
                    self.trigger.emit({'data': f"-----实卡{phone},未注册 ，现在开始注册 ----- ", "kind": "message"})
                    self.start_register(phone)
                if cookie:
                    return (phone, pwdd, cookie, project_id)
                else:
                    continue
        
    def juge_cookie(self, cookie):
        with open('name.txt', 'r', encoding='gbk') as f:
            name_list = f.readlines()
            name_list = [i for i in name_list if i]
        now_name = choice(name_list).strip()
        headers = {
            'cookie': cookie,
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
            'referer': 'https://h5.weidian.com/',
            'accept': 'application/json, */*',
        }
        p_name = parse.quote(now_name)
        res2 =  self.myrequests(url=f'https://thor.weidian.com/passport/modify.info/1.0?param=%7B%22nickname%22%3A%22{p_name}%22%7D&wdtoken=7bff0698&_=1661217586358',method="GET",  headers=headers)
        msg = res2.json()['status']['message']
        logger.info(f'cookie验证结果-------------{msg}')
        if msg == 'OK':
            return True
        elif msg == '该手机号未注册':
            return '该手机号未注册'
            
        else:
            return False
    def get_ticket(self, app_id):
        try:
            logger.info(f'开始识别滑块------------app_id: {app_id}')
            url = "https://service-gnxwxmi7-1301252454.sh.apigw.tencentcs.com/v1/csxw/solve"
            
            payload = "{\"timeout\":60,\"type\":\"tencent\",\"detail\":{\"app_id\":\"%s\"},\"author\":2}"%(app_id)
            headers = {
                'csxw-api_token': '37635:e3b5e741f29e47bd9a051bb5f7c62198:36605df6353b9ffa5652ac72e1097cac',
                'Date': 'Wed, 13 Apr 2022 11:26:02 GMT',
                'Authorization': 'hmac id="AKIDoq1kgcy35ndwp6igeUh252Dyel55hAsQGi1o", algorithm="hmac-sha1", headers="date csxw-api_token", signature="3D7ioqKJs5HFCZjc4s+0xMJtIok="'
            }
            
            response = requests.request("POST", url, timeout=240, data=payload, headers=headers)
            if response.json()['code'] == 20000:
                randstr = response.json()['data']['detail']['randstr']
                ticket = response.json()['data']['detail']['ticket']
                return  randstr, ticket
            else:
                print('----滑块识别错误-----' , response.text)
                return False, False
        except:
        
            return False, False
    
    
    def get_virtual_phone(self):
        #16257591984
        self.trigger.emit({'data': f"----开始获取虚卡, 请耐心等待--------", "kind": "message"})
    
        for i in range(1000):
            try:
                # self.virtual_auto_phone.release_phone(self.virtual_project_id)
                res_json = self.virtual_auto_phone.get_phone_number(self.virtual_project_id)
                if res_json.get("message", "") == "目前没有满足要求的卡号" and not res_json.get("data"):
                    self.trigger.emit({'data': json.dumps(res_json, ensure_ascii=False), "kind": "warn"})
                    return
                    
                try:
                    self.virtual_phone = res_json['mobile']
                    int(self.virtual_phone)
                    # self.trigger.emit({'data': f"-----得到虚卡{virtual_phone}----- ", "kind": "message"})
            
                except:
                    logger.info('--------获取虚卡发生错误----')
                    self.trigger.emit({'data':  f"此{self.virtual_auto_phone.category}，项目id：{self.virtual_project_id}没有获取到手机号", "kind": "warn"})
                    return
                if self.phone_is_registed(self.virtual_phone):
                    # logger.info(f'--------获取虚卡{self.virtual_phone}已经注册----')
                
                    self.virtual_auto_phone.shield_phone(self.virtual_project_id, self.virtual_phone)
                    self.virtual_auto_phone.release_phone(self.virtual_project_id, self.virtual_phone)
                    # self.trigger.emit({'data': f"----虚卡{virtual_phone}已经注册了", "kind": "message"})
                    print('========')
                
                else:
                    self.trigger.emit({'data': f"----得到未注册虚卡{self.virtual_phone}", "kind": "message"})
                    
                    return self.virtual_phone
            except:
                logger.info('--------获取虚卡发生错误,重试----')
                continue

    def change_phone(self,deal_phone, password, project_id,  cookie, count):
        self.trigger.emit({'data': {"phone": deal_phone, "count": count}, "kind": "change"})
        # self.auto_phone.release_phone(project_id, deal_phone)
        phone_ = ''
        for i in range(5):
            phone_res = self.auto_phone.get_phone_number(project_id, phone_number=deal_phone)
            if  '号码已被占用' in phone_res['message']:
                self.trigger.emit({'data': f"----当前实卡{deal_phone}被占用了， 正在等待释放中", "kind": "message"})
                time.sleep(30)
                continue
            else:
                phone_ = phone_res.get('mobile')
                break
        try:
            int(phone_)
            self.phone = phone_
            self.project_id = project_id
        except:
            upsql  = '''update real_phone_log set  is_banned==3  where phone="%s"'''%(deal_phone)
            self.c.execute(upsql)
            self.conn.commit()
            self.trigger.emit({'data': {"status":'换绑结束,平台没有得到实卡', "count": count, 'phone': deal_phone
                                        }, "kind": "change"})
            self.trigger.emit({'data': f"----因为没有得到可用得实卡此次换绑结束", "kind": "message"})
            return
        virtual_phone = self.get_virtual_phone()
        if not virtual_phone:
            self.trigger.emit({'data': {"status":'换绑结束,无可用虚卡', "count": count, 'phone': deal_phone}, "kind": "change"})
        
            self.trigger.emit({'data': f"----因为没有得到可用得虚卡此次换绑结束", "kind": "message"})
            return
            
        cuid = ''.join([random.choice(self.pwd_str) for i in range(32)])
        
        headers = {
            'Accept': 'application/json, */*',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 9; MI 8 Lite Build/PKQ1.181007.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobile Safari/537.36  WDAPP(WDBuyer/5.9.7) appid/com.koudai.weidian.buyer KDJSBridge2/1.1.0',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'X-Requested-With': 'com.koudai.weidian.buyer',
            'Referer': 'https://sso.weidian.com/login/modify/index.php?wfr=profile',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
            'Cookie': cookie
            
        }
        def deal_fisrt_phone(deal_phone, phone_type=True):
            #https://thor.weidian.com/passport/get.vcode/2.0
            #https://thor.weidian.com/passport/get.vcode/2.0
        
            url = 'https://thor.weidian.com/passport/get.vcode/2.0'
            submit_verify = 'https://thor.weidian.com/passport/get.vcode/2.0'
            
            RandStr, slideTicket = '', ''
            aid = ''
            wdtoken = '7cad505b' if phone_type else  "7bff0698"
            for _ in range(1, 6):
                
                
                data = {
                    #{"cuid":"011b6ff84f1732fa3e66dc01fbb0de0f","suid":"011b6ff84f1732fa3e66dc01fbb0de0f","appid":"com.koudai.weidian.buyer"}
                    #{"cuid":"011b6ff84f1732fa3e66dc01fbb0de0f","suid":"011b6ff84f1732fa3e66dc01fbb0de0f","appid":"com.koudai.weidian.buyer"}
                    'param':	str({"phone":deal_phone,"countryCode":"86","action":"weidian","scene":"H5ModifyPhone"}),
                    'context':	str({"cuid":cuid,"suid":cuid,"appid":"com.koudai.weidian.buyer"}),
                    'wdtoken':	wdtoken
                    
                }
                res = requests.post(url=url, data=data, headers=headers)
                aid = res.json()['result']
    
                self.trigger.emit({'data': {"status":f'正在识别滑块{_}次', "count": count, 'phone': self.phone}, "kind": "change"})
                RandStr, slideTicket = self.get_ticket(app_id=aid)
                if RandStr:
                    break
                else:
                    continue
            data2 = {
                
                'param':	str({"phone":"%s"%deal_phone,"countryCode":"86","action":"weidian","scene":"H5ModifyPhone","slideImageAppId":"%s" %aid,"slideTicket":"%s"%slideTicket,"slideRandStr":"%s" % RandStr}),
                'context':	str({"cuid":"%s"%cuid,"suid":"%s"%cuid,"appid":"com.koudai.weidian.buyer"}),
                'wdtoken':	wdtoken
                
            }
            logger.info(f'--------触发短信验证码参数{data2}')
            
            submit_code_url = 'https://thor.weidian.com/passport/modifyPhone.checkVcode/1.0'
            if phone_type:
                res3 = requests.post(url=submit_verify, data=data2, headers=headers)
                print(res3.text)
                sms_code = self.get_sms_code()
            else:
                sms_code = ''
                for _ in range(3):
                    try:
                        get_virtual_code = collectSms(user_name=self.virtual_user_name, passwd=self.virtual_passwd, category=self.virtual_catrgory)
                        #释放虚卡
                        self.virtual_auto_phone.release_phone(self.virtual_project_id, deal_phone)
                        # test.release_phone(project_id, phone)
                        get_virtual_code.get_phone_number(self.virtual_project_id, phone_number=deal_phone)
                        time.sleep(3)
                        res3 = requests.post(url=submit_verify, data=data2, headers=headers)
                        print(res3.text)
                        sms_code = get_virtual_code.test_send_code(self.virtual_project_id, deal_phone)
                    except:
                        pass
                    else:
                        break
                    
                
            if not sms_code:
                self.trigger.emit({'data': f"-------操作失败 ， 因为: {deal_phone}没有收到短信验证码-- ---", "kind": "message"})
                return 'not_code'
            
            else:
                self.trigger.emit({'data': f"----得到 {deal_phone}短信验证码：{sms_code}", "kind": "message"})
            
                data3 = {
                    'param'	:str({"phone":"%s"%deal_phone,"countryCode":"86","vcode":"%s"%sms_code}),
                    #{"cuid":"011b6ff84f1732fa3e66dc01fbb0de0f","suid":"011b6ff84f1732fa3e66dc01fbb0de0f","appid":"com.koudai.weidian.buyer"}
                    'context':	str({"cuid":"%s"% cuid,"suid":"%s"%cuid,"appid":"com.koudai.weidian.buyer"}),
                    'wdtoken':	wdtoken
                    
                }
                res4 = requests.post(url=submit_code_url, headers=headers, data=data3)
                logger.info('-提交短信验证码结果: %s----------', res4.text)
                return res4
        
        self.trigger.emit({'data': {"status":'实卡解绑中', "count": count, 'phone': deal_phone}, "kind": "change"})
        first_response = deal_fisrt_phone(phone_)

        if first_response and  first_response.json()['status']['message'] == 'OK':
            self.trigger.emit({'data': "---实卡解绑成功， 开始进行虚拟卡绑定-----", "kind": "message"})
            print(first_response.headers,'-----------cokie')
            self.trigger.emit({'data': {"status":'绑定虚卡中', "count": count, 'phone': deal_phone}, "kind": "change"})
            check_url = 'https://sso.weidian.com/user/bindcheck'
            check_data = {
                'phone':	virtual_phone,
                'countryCode':	'86',
                'ua':	'H4sIAAAAAAAAA%2B1VibHcIAxtSUIXlMNZRYrPE979m59MCshM7MGSQedDIIo6iYhF8C2vgccafX9YisxzOaqXVMiwzzXHI0BWprVus5bDyyQO91HPCF6zydpt0%2B%2FP5tpEnm9XqRUs14xhuy4%2F0b35xMvliJUtWCpVJ2Z3WFQZhZQdwlH8YFYLSbuzSxv4Cg7SoW7gSti1p%2BUY6TAWhk2WikF4Iz3g5bQQEV5a2g8KgpZZuHnRkfYyHpGrIeI3qqHVFH5nmSXlm4%2FrNbUE%2FElr5m5RQrB6fMOKPlmpeL05LOgciSeWa72K39yblMwLMuVmAdsmzu7O0Gx%2FREQWqspGhuguj%2Fyjpf8Xmg0jMip%2Fckz8DmJFFGVnxMBL4QFykVlgHQN4Y%2FVjC0jCdjekknFABrKSPi66mccvj2M0qhI2Vl19MzXPnDa8CWJg%2BDfqkZntkLLA2bXrSOjDDy2IOnFNPP1VJ0YBJBH3lcKXmlZEPiNxz%2Bhhl25twWP6OlQtvaLG7jriILPksBMhNP2r2i5OxVe4VM9TU7%2FtGokiqsDfAQJ3fzTtSdlr%2F6%2Fef7h6q%2B6sHuQt6etW2bfbCbUw9W91Yh8MUffl7pHcfcKe5ZGQQa8rFMuxS5WlyR1gNPFXKq1JnV7cbu1ym1p75nBrHsiFzqltW3K4e3f1ywFYo8sp8Vp59GgsHafY22vOzJm%2FZt16Lz1LFW9D6W6AxAW6ZRBvpJ9DFRi8B%2FBgWvhItY1CwfE9e64muuu2jYIS9tsiUH8%2FSHqy2UU6zZ0VOjmCmJvyGgR95YEsBP0FbagMy%2BwTKB2TWKE79IvebhUwlv0F618UTnp2sQ6ZPj4U85xXUnazUz40kWipH4%2Fsm77sXx3ov%2BnbDucmDv6iOT%2BrPnF0%2B1BkItlAZaG9tQ99y29A1M6H%2Fng2Zi3n8xPbFURQpwcAAA%3D%3D'
            }
            check_res = requests.post(url=check_url, headers=headers, data=check_data)
            print('--------------check_res', check_res.text)
            second_response = ''
            for _ in range(5):
                
                second_response = deal_fisrt_phone(virtual_phone, phone_type=False)
                #虚卡没有收到短信
                if second_response == 'not_code':
                    self.trigger.emit({'data': f"---虚卡{virtual_phone} 没有收到短信， 更换虚卡操作-----", "kind": "message"})
                    virtual_phone = self.get_virtual_phone()
                    if not virtual_phone:
                        self.trigger.emit({'data': {"status":'换绑结束,无可用虚卡', "count": count, 'phone': deal_phone}, "kind": "change"})
                    
                        self.trigger.emit({'data': f"----因为没有得到可用得虚卡此次换绑结束", "kind": "message"})
                        return
                    continue
                else:
                    break
            if second_response and second_response.json()['status']['message'] == 'OK':
                recover_url = 'https://thor.weidian.com/passport/modifyPhone.recoverPhone/1.0'
                recover_data  = {
                    'param':	str({"newCountryCode":86,"newPhone":"%s"%virtual_phone}),
                'context':	str({"cuid":"%s"%cuid,"suid":"%s"%cuid,"appid":"com.koudai.weidian.buyer"}),
                'wdtoken':	'e562a7fc'
                
                }
                res_recover = requests.post(url=recover_url, headers=headers, data=recover_data)
                logger.info('---------recoverPhone-: %s-----', res_recover.text)
                self.trigger.emit({'data': "---换绑成功-----", "kind": "message"})
                self.phone = virtual_phone
                change_cookie = self.login(password)
                if change_cookie:
                    self.trigger.emit({'data': {"status":'换绑获取cookie成功', "count": count, 'phone': self.phone}, "kind": "change"})
                
                    if self.lineEdit_shiming_retry:
                        self.trigger.emit({'data': f"---开始对虚卡：{deal_phone}-- 进行实名操作---", "kind": "message"})
                        #调用实名操作
                        cert_info = self.authentication(change_cookie, password)
                        if cert_info:
                            self.trigger.emit({'data': f"---虚卡：{virtual_phone}-- 实名成功---", "kind": "message"})
                            certNo, userName = cert_info.get('certNo'), cert_info.get('userName')
                            self.save_data2text(self.phone+'|'+password+'|'+change_cookie+'}}}'+certNo+'}}}'+userName + '\n', '换绑成功的cookie记录.txt')
                    
                        else:
                            self.save_data2text(self.phone+'|'+password+'|'+change_cookie + '\n', '换绑成功的cookie记录.txt')
                            self.trigger.emit({'data': f"---虚卡：{deal_phone}-- 实名失败---", "kind": "message"})
                        
                        
                        
                        
                    else:
                        self.save_data2text(self.phone+'|'+password+'|'+change_cookie + '\n', '换绑成功的cookie记录.txt')
                        self.trigger.emit({'data': f"---虚卡：{deal_phone}-- 不进行实名操作---", "kind": "message"})
                    self.trigger.emit({'data': f"---开始重新注册实卡：{deal_phone}-----", "kind": "message"})
                    self.trigger.emit({'data': {"status":'重新注册实卡', "count": count, 'phone': deal_phone}, "kind": "change"})
                    self.start_register(deal_phone)
                    return  True
                    
                else:
                    self.trigger.emit({'data': "---换绑时， cookie获取失败-----", "kind": "message"})
                    self.trigger.emit({'data': {"status":'cookie获取失败', "count": count, 'phone': self.phone}, "kind": "change"})
    
    
    
            else:
                self.trigger.emit({'data': "---虚卡绑定失败-----", "kind": "message"})
                return
        else:
            self.trigger.emit({'data': "---实卡解绑失败-----", "kind": "message"})
            return



def except_hook(cls, exception, traceback):
    
    sys.__excepthook__(cls, exception, traceback)



if __name__ == "__main__":
    project_debug = False
    sys.excepthook = except_hook
    app = QApplication(sys.argv)
    m = MainWindow()
    m.show()
    sys.exit(app.exec_())
    #------------------

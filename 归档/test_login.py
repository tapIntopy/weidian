import logging as logger
import base64, json, io
import time
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
import random
import gzip
import requests

from  hashlib import md5

mc_random = ["a","1","b","3","4","5","6","f","8","9"]

def get_random_mc():
	mc = '{}:{}:{}:{}:{}:{}'.format("".join(random.choices(mc_random,k=2)),"".join(random.choices(mc_random,k=2)),"".join(random.choices(mc_random,k=2)),"".join(random.choices(mc_random,k=2)),"".join(random.choices(mc_random,k=2)),"".join(random.choices(mc_random,k=2)))
	return mc

class Login():
	def __init__(self, phone):
		self.pwd_str = ['1','2', '7', '0', '3', '4', '8', '5', 'q', 'r', 'e', 'h', 'n']
	
		self.phone = phone
		self.nKey="6cec4a8b268b8749dfa0bd409effcd08"
		self.content = self.get_content()
		self.headers = {
			"Host": "thor.weidian.com",
			"x-schema": "https",
			"referrer": "https://android.weidian.com",
			"referer": "https://android.weidian.com",
			"origin": "android.weidian.com",
			"x-origin": "thor",
			"x-encrypt": "1",
			"user-agent": "Android/10 WDAPP(WDBuyer/5.9.7) Thor/2.3.3",
			"content-type": "application/x-www-form-urlencoded; charset=utf-8",
			"accept-encoding": "GLZip",  # 影响到结果是否解压
		}
	def get_content(self):
		mc = get_random_mc()
		cuid = ''.join([random.choice(self.pwd_str) for i in range(32)])
		android_id = ''.join([random.choice(self.pwd_str) for i in range(16)])
		content = '{"alt":"","android_id":"%s","app_status":"active","appid":"com.koudai.weidian.buyer","appv":"5.9.7","brand":"google","build":"20200408164901","channel":"1013n","cuid":"%s","disk_capacity":"16.60GB/24.32GB","feature":"E|F,H|F,P|F,R|T","h":"1794","iccid":"","imei":"","imsi":"","lat":"","lon":"","mac":"%s","machine_model":"armv8l","memory":"1696M/3765M","mid":"Pixel","mobile_station":"0","net_subtype":"0_","network":"WIFI","oaid":"","os":"29","platform":"android","serial_num":"unknown","suid":"%s","w":"1080","wmac":"02:00:00:00:00:00","wssid":"<unknown ssid>"}'%(android_id,cuid, mc, cuid )
		return content
	@staticmethod
	def get_sign_buyerPassword(password):
		str1 = base64.b64encode(password.encode()).decode()
		i = 0
		s = ""
		while i < len(str1):
			s += chr(ord(str1[i]) + (i % 3))
			i += 1
		s = s + "koudai_gouwu_is_success_for_ever"
		return md5(s.encode()).hexdigest()
	def get_aes_key(self):
		v6 = self.nKey
		v10 = v6[1:]
		v10_index = 0
		v8 = len(v6)
		v11 = v8 >> 1
		aesKey = []
		while v11 > 0:
			if v10_index == 0:
				v13 = ord(v6[v10_index])
			else:
				v13 = ord(v10[v10_index - 1])
			v14 = ord(v10[v10_index])
			v10_index += 2
			if v13 > 0x39:
				v13 = v13 + 9
			if v14 > 0x39:
				v14 = v14 + 9
			v11 = v11 - 1
			aesKey.append(hex((v14 & 0xf | v13 << 4) & 0xff))
		return ''.join(x[2:].zfill(2) for x in aesKey)
	
	def aes_decrypt_decompress(self, gzip_data, key=None) -> str:
		if key is None:
			key = self.get_aes_key()
		key = bytearray.fromhex(key)
		padtext = gzip_data
		# padtext = pad(gzip_data, 16, style='pkcs7')
		aes = AES.new(key, AES.MODE_ECB)
		message = aes.decrypt(padtext).rstrip()
		# print(message)
		# print(message.split(b"\x00\x00\x00"))
		try:
			message1 = b"\x00\x00\x00".join(message.split(b"\x00\x00\x00")[:-1]) + b"\x00\x00\x00"
			return gzip.decompress(message1).decode()
		except:
			message2 = b"\x00\x00".join(message.split(b"\x00\x00")[:-1]) + b"\x00\x00"
			return gzip.decompress(message2).decode()
	@staticmethod
	def gzip_compress(buf):
		if isinstance(buf, dict):
			buf = json.dumps(buf, separators=(",", ":"))
		out = io.BytesIO()
		buf = buf.encode()
		with gzip.GzipFile(fileobj=out, mode="w") as f:
			f.write(buf)
		return out.getvalue()
	
	def aes_encrypt_compress(self, message, key=None):
		gzip_data = self.gzip_compress(message)
		if key is None:
			key = self.get_aes_key()
		key = bytearray.fromhex(key)
		padtext = pad(gzip_data, 16, style='pkcs7')
		aes = AES.new(key, AES.MODE_ECB)
		message = aes.encrypt(padtext)
		base64_encrypted = base64.b64encode(message)
		return base64_encrypted.decode()
	@staticmethod
	def get_sign(data, salt="2a7d5d1d3c0a4df08bfbacd58fb0748d") -> str:
		data_sort = sorted(data, key=lambda d: d[0])
		sign_str = ""
		for k in data_sort:
			sign_str += k + "=" + data[k] + "&"
		sign_str += salt
		sign_1 = md5(sign_str.encode()).hexdigest().upper()
		sign_2 = md5((sign_1 + salt).encode()).hexdigest().upper()
		return sign_2
	
	def get_encrypt_data(self, param_plain: dict) -> dict:
		param = self.aes_encrypt_compress(json.dumps(param_plain, separators=(",", ":")))
		if isinstance(self.content, dict):
			context_plant = json.dumps(self.content, separators=(",", ":"))
		else:
			context_plant = self.content
		context = self.aes_encrypt_compress(context_plant)
		timestamp = str(int(time.time() * 1e3))
		data = {
			"param": param,
			"v": "2.0",
			"context": context,
			"appkey": "06475281",
			"timestamp": timestamp,
		}
		sign = self.get_sign(data)
		data["sign"] = sign
		return data
	
	@staticmethod
	def get_sign_sellerPassword(password):
		s = "_Wedian&&Is##Wonderful**@~0_" + password
		return md5(s.encode()).hexdigest()
		
		
		
	def get_certNo(self):
		with open('id_card.txt', 'r', encoding='utf-8') as f:
				result = f.readlines()
		idcard_list = []
		for ic in result:
			now_icar = {}
			icard_info = ic.strip().split('----')
			
			id_card_flag = icard_info[2] if len(icard_info)>2 else 0
			
			now_icar['certNo'], now_icar['userName'], flag  = icard_info[1], icard_info[0], id_card_flag
			if id_card_flag>=0:
				idcard_list.append(now_icar)
		return idcard_list
	def update_certno(self, cerNo, info):
		f = open('id_card.txt','r', encoding='utf-8')
		a = f.readlines()
		f = open('id_card.txt','w', encoding='utf-8')
		for i in a:
			if cerNo in i:
				content_list = i.split('----')
				if len(content_list)>2:
					if info==-1:
						write_content = '----'.join(content_list[0:2]).strip()+'----'+'-1' +'\n'
					else:
						now_number = int(content_list[-1]) + 1
						write_content = '----'.join(content_list[0:2]).strip()+'----'+f'{now_number}' +'\n'
				
				else:
					write_content = i.strip() +f'----{info}'  +'\n'
				f.write(write_content)
			else:
				f.write(i)
		
		f.close()
		
		
		
		
	def authentication(self, cookie, pwd):
		headers = {
			'User-Agent': 'Mozilla/5.0 (Linux; Android 9; MI 8 UD Build/PKQ1.180729.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/102.0.5005.125 Mobile Safari/537.36  WDAPP(WDBuyer/5.9.7) appid/com.koudai.weidian.buyer KDJSBridge2/1.1.0',
			'Cookie': cookie,
			'Content-Type': 'application/x-www-form-urlencoded',
			'Referer': 'https://weidian.com/',
			'X-Requested-With': 'com.koudai.weidian.buyer'
			
		}
		fial_time = 0
		def authen():
			self.icard_list = self.get_certNo()
			global fial_time
			timestamp = str(int(time.time() * 1e3))
			get_no = 'https://thor.weidian.com/authentication/realname.createAuthOrder/1.0'
			get_data = {'v': '1.0',
						'timestamp': timestamp}
			res_no = self.myrequests(url=get_no, method="POST",  data=get_data, headers=headers)
			logger.info(res_no.text)
			if 'LOGIN ERROR' in res_no.text:
				logger.warning('---------需要重新登录-phone: %s----', self.phone)
				cookie = self.login(pwd)
				headers['Cookie'] = cookie
			elif '已完成实名认证请勿重复认证' in res_no.text:
				logger.info('%s---->此账号已经实名------------', self.phone)
				return {'certNo': '默认实名', 'userName': '默认实名'}
			
			requestNo = json.loads(res_no.text)['result']['requestNo']
			
			
			now_idcard = self.icard_list.pop()
			certNo, userName  = now_idcard.get('certNo'), now_idcard.get('userName')
			url = 'https://thor.weidian.com/authentication/realname.submitAuthOrder/1.0'
			data = {"param":str({"certType":"IC","certNo":f"{certNo}","userName":f"{userName}","phone":"","bankNo":"","cvv":"","ValidDate":"","frontImg":"","handImg":"","authSource":"h5","authType":"iclevel2","requestNo":f"{requestNo}"}),
					'v': '1.0',
					'timestamp':    timestamp
				
					}
			logger.info(data)
			res = self.myrequests(url=url,method="POST", headers=headers, data=data)
			logger.info(res.content.decode())
			
			if '认证失败' in res.text:
				fial_time+=1
				fail_card = f'{userName}----{certNo}----failed\n'
				self.save_data2text(fail_card,'fail_card.txt')
				logger.warning('-------当前身份证不可用， 开始更换身份证-----------')
				self.update_certno(certNo, -1)
				time.sleep(1)
				if fial_time>10:
					return False
				else:
					return authen()
			elif 'LOGIN ERROR' in res.text:
				logger.warning('---------需要重新登录-phone: %s----', self.phone)
				cookie = self.login(pwd)
				headers['Cookie'] = cookie
				return authen()
			elif '该账号存在风险，您的申请不予通过' in res.text:
				logger.warning('%s---->经平台安全系统检测，该账号存在风险，您的申请不予通过----------', self.phone)
			elif '实名认证次数超限,请在1小时后重试' in res_no.text:
				logger.warning('%s---->实名认证次数超限,请在1小时后重试----------', self.phone)
				return False
			else:
				self.trigger.emit({'data': f"实名成功 phone: {self.phone}, 身份证:{certNo}----名字为：{userName}", "kind": "message"})
				self.update_certno(certNo, 1)
				return now_idcard
		return authen()
	def fetch_vcode(self, param_plain: dict, register_url=None) -> dict:
		#更新代理
		# https://thor.weidian.com/passport/get.vcode/1.0
		url = "https://thor.weidian.com/passport/get.vcode/1.0" if not register_url else register_url
		data = self.get_encrypt_data(param_plain)
		for i in range(3):
			try:
				response = requests.post(url=url,   headers=self.headers, data=data)
				print(response.headers)
				break
			except Exception as e:
				print(e)
				pass
		else:
			print("重试请求失败e=")
			return
		response_message = self.aes_decrypt_decompress(response.content)
		# self.trigger.emit({'data': f"{response_message}", "kind": "message"})
		print(f"{response_message}")
		return json.loads(response_message)
	def login(self, pwd):
		logger.info('--phone: %s----pwd: %s-开始的登录----', self.phone, pwd)
		buyerPassword = self.get_sign_buyerPassword(pwd)
		sellerPassword = self.get_sign_sellerPassword(pwd)
		login_data = {"buyerPassword":f"{buyerPassword}","countryCode":"86","phone":self.phone,"sellerPassword":f"{sellerPassword}"}
		login_url = 'https://thor.weidian.com/passport/login.mobile/1.0'
		login_res = self.fetch_vcode(login_data, login_url)
		return login_res
	
	
	
	def myrequests(self,url, method, data=None, json=None, **kwargs):
		
		for i in range(10):
			try:
				if i >5:
					proxies = {}
				if method == 'GET':
					return requests.get(url,   timeout=6, **kwargs)
				elif method == "POST":
					return requests.post(url, data=data, json=json,timeout=6,   **kwargs)
				else:
					logger.error("--------请求类型需要适配")
					return
			except:
				pass


'''
{"status":{"code":50,"subCode":1,"message":"redirect to other page","description":"请实名认证后安全使用微店"},"result":{"redirect":"https://h5.weidian.com/m/identity-auth/index.html"}}


'''
test = Login('17629131671')
pwd = 'xw3344'
res = test.login(pwd)
print(res)


# if res['status']['description'] == '请实名认证后安全使用微店':
# 	test.authentication()
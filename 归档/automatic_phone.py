import time
import requests
from log import logger

class collectSms():
	def __init__(self,user_name='', passwd='', category='椰子云'):
		self.category = category
		self.user_name = 'xw575861164' if category in ['椰子云', '蓝泰云'] else 'MY.46014023'
		self.passwd = 'xw575861164'
		if user_name:
			self.user_name = user_name
		if passwd:
			self.passwd = passwd
		self.token = self.login()
		self.money = self.get_money_count()
	#得到手机号
	def get_phone_number(self, project_id,  phone_number=None):
		lty_url = f'http://api.lx967.com:9090/sms/api/getPhone?token={self.token}&sid={project_id}&ascription=2'
		miyun_url = f'https://api.miyun999.live/api/get_mobile?token={self.token}&project_id={project_id}&operator=4'
		yz_url = f'http://api.jinpaocun.net:81/api/get_mobile?token={self.token}&project_id={project_id}'
		if self.category=='椰子云':
			url = yz_url
		elif self.category=='蓝泰云':
			url = lty_url
		else:
			url = miyun_url
		if phone_number:
			url = url + f'&phone_num={phone_number}'
		res = requests.get(url=url)
		print(res.text)
		# mobile = res.json()['mobile']
		data = res.json()
		if data.get('phone'):
			data['mobile'] = data.get('phone')
		return data
	def login(self):
		lty_url = f'http://api.lx967.com:9090/sms/api/login?username={self.user_name}&password={self.passwd}'
		miyun_url = f'https://api.miyun999.live/api/login?apiName={self.user_name}&password={self.passwd}'
		yz_url = f'http://api.jinpaocun.net:81/api/logins?username={self.user_name}&password={self.passwd}'
		if self.category=='椰子云':
			url = yz_url
		elif self.category=='蓝泰云':
			url = lty_url
		else:
			url = miyun_url
		res = requests.get(url=url)
		print(res.text)
		token = res.json().get('token')
		return token

	def get_money_count(self):
		lty_url = f'http://api.lx967.com:9090/sms/api/getMessage?token={self.token}'
		miyun_url = f'http://api.miyun999.live/api/get_myinfo?token={self.token}'
		yz_url = f'http://api.jinpaocun.net:81/api/get_myinfo?token={self.token}'
		if self.category=='椰子云':
			url = yz_url
		elif self.category=='蓝泰云':
			url = lty_url
		else:
			url = miyun_url
		res = requests.get(url=url)
		try:
			money = res.json()['data'][0]['money'] if self.category=='椰子云' else res.json()['money']
			return money
		except:
			return res.text
	def test_send_code(self, project_id, phone_num):
		#获取短信
		lty_url = f'http://api.lx967.com:9090/sms/api/getMessage?token={self.token}&sid={project_id}&phone={phone_num}'
		miyun_url = f'https://api.miyun999.live/api/get_message?token={self.token}&project_id={project_id}&phone_num={phone_num}'
		yz_url = f'http://api.jinpaocun.net:81/api/get_message?token={self.token}&project_id={project_id}&phone_num={phone_num}'
		for i in range(10):
			time.sleep(10)
			if self.category=='椰子云':
				url = yz_url
			elif self.category=='蓝泰云':
				url = lty_url
			else:
				url = miyun_url
			res = requests.get(url=url)
			logger.info("手机号： %s----->%s", phone_num, res.text )
			code = res.json().get('code')
			if code:
				return code
	def get_sms(self, project_id, phone_num):
		lty_url = f'http://api.lx967.com:9090/sms/api/getMessage?token={self.token}&sid={project_id}&phone={phone_num}'
		miyun_url = f'https://api.miyun999.live/api/get_message?token={self.token}&project_id={project_id}&phone_num={phone_num}'
		yz_url = f'http://api.jinpaocun.net:81/api/get_message?token={self.token}&project_id={project_id}&phone_num={phone_num}'
		if self.category=='椰子云':
			url = yz_url
		elif self.category=='蓝泰云':
			url = lty_url
		else:
			url = miyun_url
		res = requests.get(url=url)
		logger.info("手机号： %s----->%s", phone_num, res.text )
		if url ==lty_url:
			code =  res.json().get('sms')
		else:
			code = res.json().get('code')
		return code
	
	
	#拉黑手机号
	def shield_phone(self, project_id, phone_num):
		lty_url = f'http://api.lx967.com:9090/sms/api/addBlacklist?token={self.token}&sid={project_id}&phone={phone_num}'
		miyun_url = f'http://api.miyun999.live/api/add_blacklist?token={self.token}&project_id={project_id}&phone_num={phone_num}'
		yz_url = f'http://api.jinpaocun.net:81/api/add_blacklist?token={self.token}&project_id={project_id}&phone_num={phone_num}'
		if self.category=='椰子云':
			url = yz_url
		elif self.category=='蓝泰云':
			url = lty_url
		else:
			url = miyun_url
		res = requests.get(url=url)
		message = res.json()['message']
		logger.info('%s---->拉黑手机号----%s', phone_num, res.text)
	#释放手机号
	def release_phone(self, project_id, phone_num):
		lty_url = f'http://api.lx967.com:9090/sms/api/cancelRecv?token={self.token}&sid={project_id}&phone={phone_num}'
		miyun_url = f'http://api.miyun999.live/api/free_mobile?token={self.token}n&project_id={project_id}&phone_num={phone_num}'
		yz_url = f'http://api.jinpaocun.net:81/api/free_mobile?token={self.token}&project_id={project_id}&phone_num={phone_num}'
		if self.category=='椰子云':
			url = yz_url
		elif self.category=='蓝泰云':
			url = lty_url
		else:
			url = miyun_url
		res = requests.get(url=url)
		message = res.json()['message']
		logger.info('%s---->释放手机号---->%s', phone_num,  message)


if __name__=='__main__':
	test = collectSms(category='米云', user_name='MY.1327691919')
	#"10375----2358Q0"
	project_id = '10762'
	phone = '15523429148'
	# test.release_phone(project_id, phone)
	test.get_phone_number(project_id, phone)
	test.test_send_code(project_id, phone)